# Projet différencié
## Serveur de traitement d'images

## Dépendances
* CMake >= 3.0  
* Vagrant >= 1.5  
* VirtualBox >= 4.0  
* DoxyGen >= 1.8  

## Installation

Regarder en premier le fichier 'Vagrantfile' pour savoir s'il est adapté à votre configuration (cf. config.vm.provider). La box que nous utilisons est scotchbox [Official WebSite](https://box.scotch.io/). Il s'agit d'une box Ubuntu avec LAMP pré-installé, ainsi que NetPBM (la librairie utilisée pour les convertions d'image).  
Évidement, l'ensemble de l'application web peut tout aussi bien être déployé sur un serveur dédié.  
Pour lancer la box, taper simplement 'vagrant up' à la racine du projet.  
Pour compiler le programme, nous utilisons cmake. Voici une "One Command" à taper à la racine du projet.  
* (cd public/app && mkdir build ; cd build && cmake .. && make && make doc && cd latex && make)  

Cette commande compile le programme et génére la documentation.  
Pour stopper la box Vagrant, taper 'vagrant halt'.  
Pour y accéder en ssh, taper 'vagrant ssh'.

Le script install.sh réalise toute l'installation.public/app/build

## Usage

Pour acceder à l'application Web, ouvrez l'URL suivant [192.168.33.10](http://192.168.33.10/).

## Facultatif

Pour pouvoir envoyer sur le serveur des images de plus de 8Mo modifier la variable 'post_max_size = 8M' par 'post_max_size = 512M' et la variable 'upload_max_filesize = 2M' par 'upload_max_filesize = 512M' dans le fichier '/etc/php5/apache2/php.ini' via le ssh de vagrant.
