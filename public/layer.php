<?php
    session_start();
    if (isset($_POST['layers'])) {
        exec('mv '.$_SESSION['fileviewprocess'].' '.$_SESSION['fileview']);
        if (strrchr($_SESSION['fileprocess'],'.') != strrchr($_SESSION['filepixmap'],'.')) {
            $ext_proc = strrchr($_SESSION['fileprocess'],'.');
            $ext_pix = strrchr($_SESSION['filepixmap'],'.');
            switch ($ext_proc) {
                case '.ppm':
                case '.PPM':
                    switch ($ext_pix) {
                        case '.pgm':
                        case '.PGM':
                            exec('./app/build/ips --convert P5 '.$_SESSION['fileprocess'].' '.$_SESSION['user_dir'].'/process');
                            $_SESSION['fileprocess'] = $_SESSION['user_dir'].'/process'.$ext_pix;
                            break;
                        case '.pbm':
                        case '.PBM':
                            exec('./app/build/ips --convert P4 '.$_SESSION['fileprocess'].' '.$_SESSION['user_dir'].'/process');
                            $_SESSION['fileprocess'] = $_SESSION['user_dir'].'/process'.$ext_pix;
                            break;
                        default:
                            $_SESSION['error'] = 'Extension invalide';
                            break;
                    }
                    break;
                case '.pgm':
                case '.PGM':
                    switch ($ext_pix) {
                        case '.ppm':
                        case '.PPM':
                            exec('./app/build/ips --convert P6 '.$_SESSION['fileprocess'].' '.$_SESSION['user_dir'].'/process');
                            $_SESSION['fileprocess'] = $_SESSION['user_dir'].'/process'.$ext_pix;
                            break;
                        case '.pbm':
                        case '.PBM':
                            exec('./app/build/ips --convert P4 '.$_SESSION['fileprocess'].' '.$_SESSION['user_dir'].'/process');
                            $_SESSION['fileprocess'] = $_SESSION['user_dir'].'/process'.$ext_pix;
                            break;
                        default:
                            $_SESSION['error'] = 'Extension invalide';
                            break;
                    }
                    break;
                case '.pbm':
                case '.PBM':
                    switch ($ext_pix) {
                        case '.ppm':
                        case '.PPM':
                            exec('./app/build/ips --convert P6 '.$_SESSION['fileprocess'].' '.$_SESSION['user_dir'].'/process');
                            $_SESSION['fileprocess'] = $_SESSION['user_dir'].'/process'.$ext_pix;
                            break;
                        case '.pgm':
                        case '.PGM':
                            exec('./app/build/ips --convert P5 '.$_SESSION['fileprocess'].' '.$_SESSION['user_dir'].'/process');
                            $_SESSION['fileprocess'] = $_SESSION['user_dir'].'/process'.$ext_pix;
                            break;
                        default:
                            $_SESSION['error'] = 'Extension invalide';
                            break;
                    }
                    break;
                default:
                    $_SESSION['error'] = 'Extension invalide';
                    break;
            }
        }
        exec('mv '.$_SESSION['fileprocess'].' '.$_SESSION['filepixmap']);
        unset($_SESSION['fileviewprocess']);
    }
    header('Location: index.php');
?>
