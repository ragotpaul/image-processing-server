<?php
    session_start();
    if (!(file_exists('app/build/ips')) OR !(file_exists('app/build'))) {
        $_SESSION['error'] = 'Serveur en maintenance';
    } elseif (isset($_POST['process']) AND isset($_SESSION['ext']) AND isset($_SESSION['user_id']) AND isset($_SESSION['user_dir']) AND isset($_SESSION['fileview']) AND isset($_SESSION['filepixmap'])) {
        switch($_POST['process']) {
            case 'binarisation':
                $cmd = escapeshellcmd('./app/build/ips --binarisation '.$_SESSION['filepixmap'].' '.$_SESSION['user_dir'].'/process');
                break;
            case 'negatif':
                $cmd = escapeshellcmd('./app/build/ips --negatif '.$_SESSION['filepixmap'].' '.$_SESSION['user_dir'].'/process');
                break;
            case 'gray_level':
                $except = array('.ppm', '.PPM');
                if (!in_array(strrchr($_SESSION['filepixmap'],'.'), $except)) {
                    exec('./app/build/ips --convert P6 '.$_SESSION['filepixmap'].' '.$_SESSION['user_dir'].'/pixmap');
                    $cmd = escapeshellcmd('./app/build/ips --gray-level '.$_SESSION['user_dir'].'/pixmap.ppm '.$_SESSION['user_dir'].'/process');
                } else {
                    $cmd = escapeshellcmd('./app/build/ips --gray-level '.$_SESSION['filepixmap'].' '.$_SESSION['user_dir'].'/process');
                }
                break;
            case 'increase_contrast':
                $except = array('.pgm', '.PGM');
                if (!in_array(strrchr($_SESSION['filepixmap'],'.'), $except)) {
                    exec('./app/build/ips --convert P5 '.$_SESSION['filepixmap'].' '.$_SESSION['user_dir'].'/pixmap');
                    $cmd = escapeshellcmd('./app/build/ips --contrast '.$_SESSION['user_dir'].'/pixmap.pgm '.$_SESSION['user_dir'].'/process');
                } else {
                    $cmd = escapeshellcmd('./app/build/ips --contrast '.$_SESSION['filepixmap'].' '.$_SESSION['user_dir'].'/process');
                }
                break;
            case 'symetry_h':
                $cmd = escapeshellcmd('./app/build/ips --symetry-h '.$_SESSION['filepixmap'].' '.$_SESSION['user_dir'].'/process');
                break;
            case 'symetry_v':
                $cmd = escapeshellcmd('./app/build/ips --symetry-v '.$_SESSION['filepixmap'].' '.$_SESSION['user_dir'].'/process');
                break;
            case 'smooth':
                $cmd = escapeshellcmd('./app/build/ips --smooth '.$_SESSION['filepixmap'].' '.$_SESSION['user_dir'].'/process');
                break;
            case 'gradient_x':
                $cmd = escapeshellcmd('./app/build/ips --gradient-x '.$_SESSION['filepixmap'].' '.$_SESSION['user_dir'].'/process');
                break;
            case 'gradient_y':
                $cmd = escapeshellcmd('./app/build/ips --gradient-y '.$_SESSION['filepixmap'].' '.$_SESSION['user_dir'].'/process');
                break;
            case 'gradient':
                $cmd = escapeshellcmd('./app/build/ips --gradient '.$_SESSION['filepixmap'].' '.$_SESSION['user_dir'].'/process');
                break;
            case 'sobel_x':
                $cmd = escapeshellcmd('./app/build/ips --sobel-x '.$_SESSION['filepixmap'].' '.$_SESSION['user_dir'].'/process');
                break;
            case 'sobel_y':
                $cmd = escapeshellcmd('./app/build/ips --sobel-y '.$_SESSION['filepixmap'].' '.$_SESSION['user_dir'].'/process');
                break;
            case 'sobel':
                $cmd = escapeshellcmd('./app/build/ips --sobel '.$_SESSION['filepixmap'].' '.$_SESSION['user_dir'].'/process');
                break;
            case 'laplace':
                $cmd = escapeshellcmd('./app/build/ips --laplace '.$_SESSION['filepixmap'].' '.$_SESSION['user_dir'].'/process');
                break;
            case 'edge_sobel':
                $cmd = escapeshellcmd('./app/build/ips --edge-sobel '.$_SESSION['filepixmap'].' '.$_SESSION['user_dir'].'/process');
                break;
            case 'edge_laplace':
                $cmd = escapeshellcmd('./app/build/ips --edge-laplace '.$_SESSION['filepixmap'].' '.$_SESSION['user_dir'].'/process');
                break;
            case 'edge_draw':
                $cmd = escapeshellcmd('./app/build/ips --draw-edge '.$_SESSION['filepixmap'].' '.$_SESSION['user_dir'].'/process');
                break;
            default:
                $_SESSION['error'] = 'Process failed';
        }
        exec($cmd, $out);
        $_SESSION['fileprocess'] = $_SESSION['user_dir'].'/process'.strrchr($out[1],'.');
        $_SESSION['fileviewprocess'] = $_SESSION['user_dir'].'/viewprocess.png';
        exec('pnmtopng '.$_SESSION['fileprocess'].' > '.$_SESSION['fileviewprocess']);
    } else {
        $_SESSION['error'] = 'Aucune image à traiter';
    }
    header('Location: index.php');
?>
