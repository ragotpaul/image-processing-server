#ifndef BINARISATION_H
#define BINARISATION_H

void thresholding (t_img *img, int s);
void binarisation (t_img *img);

#endif
