#ifndef ADDON_H
#define ADDON_H

int send_err (char *message);
void next_line (FILE *f, int n);
int power (int n, int e);
int v_abs (int n);
double v_abs_double (double n);
int max (int x, int y);
int min (int x, int y);
int sum (int *a, int size);
double sum_double (double *a, int size);
int sum_two_array(int **a, int w, int h);
double sum_two_array_double(double **a, int w, int h);
int sum_abs (int *a, int size);
double sum_abs_double (double *a, int size);
int sum_two_array_abs (int **a, int w, int h);
double sum_two_array_abs_double (double **a, int w, int h);
int avg (int *a, int size);
int byte_decimal (int *a);
void decimal_byte (int *b, int n);
void show_array_int (int *a, int size);
void show_array_double (double *a, int size);

#endif
