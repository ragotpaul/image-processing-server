/**
 * \file img.c
 * \brief Fonctions de gestion des images pixmap chargées en mémoire
 * \version 1.0
 * \date 10/12/2015
 *
 * Contient toutes les fonctions permettant d'initialiser la structure image et de la modifier.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "config.h"
#include "type.h"
#include "addon.h"
#include "ips.h"

#include "img.h"

/**
 * \fn t_img *init_image ()
 * \brief Fonction initialisant une structure t_img avec les valeurs par default
 */

t_img *init_image () {
    t_img *img = NULL;
    img = malloc(sizeof(char[255]) + sizeof(t_extension) + sizeof(t_magicNumber) + sizeof(int) * 3 + sizeof(t_pixel) * N_PIXELS);
    if (img == NULL)
        send_err("Out of memory");
    img->name = malloc(sizeof(char[255]));
    if (img->name == NULL)
        send_err("Out of memory");
    img->a = malloc(MAX_WIDTH * sizeof(t_pixel *));
    if (img->a == NULL)
        send_err("Out of memory");
    int i = 0;
    for (i = 0 ; i < MAX_WIDTH ; i++) {
        img->a[i] = malloc(MAX_HEIGHT * sizeof(t_pixel));
        if (img->a[i] == NULL)
            send_err("Out of memory");
    }
    img->name = "";
    img->ext = 0;
    img->type = 0;
    img->w = 1;
    img->h = 1;
    img->max = 1;
    int j = 0;
    for (i = 0 ; i < MAX_WIDTH ; i++) {
        for (j = 0 ; j < MAX_HEIGHT ; j++) {
            img->a[i][j].position.x = i;
            img->a[i][j].position.y = j;
            img->a[i][j].color.red = 0;
            img->a[i][j].color.green = 0;
            img->a[i][j].color.blue = 0;
            img->a[i][j].brightness = 0;
        }
    }
    return img;
}

/**
 * \fn void free_image (t_img *img)
 * \brief Fonction liberant l'espace mémoire occupé par une structure t_img
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

void free_image (t_img *img) {
    int i = 0;
    for (i = 0 ; i < MAX_WIDTH ; i++) {
        free(img->a[i]);
    }
    free(img->a);
    free(img);
}

/**
 * \fn void copy_image (t_img *src, t_img *dst)
 * \brief Fonction copiant l'ensemble des composantes d'une structure t_img dans une autre
 * @param[out]  src Pointeur d'un t_img correspondant à l'image source
 * @param[out]  dst Pointeur d'un t_img correspondant à l'image cible
 */

void copy_image (t_img *src, t_img *dst) {
    dst->name = src->name;
    dst->ext = src->ext;
    dst->type = src->type;
    dst->w = src->w;
    dst->h = src->h;
    dst->max = src->max;
    int i = 0;
    int j = 0;
    for (j = 0 ; j < (src->h) ; j++) {
        for (i = 0 ; i < (src->w) ; i++) {
            dst->a[i][j] = src->a[i][j];
        }
    }
}

/**
 * \fn void erase_pixels (t_img *img)
 * \brief Fonction ecrasant tout les pixels d'une image en leur donnant une luminosité maximum
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

void erase_pixels (t_img *img) {
    int i = 0;
    int j = 0;
    for (i = 0 ; i < MAX_WIDTH ; i++) {
        for (j = 0 ; j < MAX_HEIGHT ; j++) {
            img->a[i][j].position.x = i;
            img->a[i][j].position.y = j;
            img->a[i][j].color.red = img->max;
            img->a[i][j].color.green = img->max;
            img->a[i][j].color.blue = img->max;
            img->a[i][j].brightness = img->max;
        }
    }
}

/**
 * \fn void clean_image (t_img *img)
 * \brief Fonction nettoyant les pixels incompatible avec le format pixmap (composantes négative ou supérieur au maximum)
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

void clean_image (t_img *img) {
    int i = 0;
    int j = 0;
    for (i = 0 ; i < MAX_WIDTH ; i++) {
        for (j = 0 ; j < MAX_HEIGHT ; j++) {
        img->a[i][j].brightness = (img->a[i][j].brightness > img->max) ? img->max : ((img->a[i][j].brightness < 0) ? 0 : img->a[i][j].brightness);
        img->a[i][j].color.red = (img->a[i][j].color.red > img->max) ? img->max : ((img->a[i][j].color.red < 0) ? 0 : img->a[i][j].color.red);
        img->a[i][j].color.green = (img->a[i][j].color.green > img->max) ? img->max : ((img->a[i][j].color.green < 0) ? 0 : img->a[i][j].color.green);
        img->a[i][j].color.blue = (img->a[i][j].color.blue > img->max) ? img->max : ((img->a[i][j].color.blue < 0) ? 0 : img->a[i][j].color.blue);
        }
    }
}

/**
 * \fn void merge_image (t_img *src, t_img *dst)
 * \brief Fonction fusionnant deux structure t_img en une, par ajout de la valeur absolu de ses composantes
 * @param[out]  src Pointeur d'un t_img correspondant à l'image source
 * @param[out]  dst Pointeur d'un t_img correspondant à l'image cible
 */

void merge_image (t_img *src, t_img *dst) {
    if (dst->h != src->h || dst->w != src->w) send_err("Different image size");
    int i = 0;
    int j = 0;
    for (i = 0 ; i < (dst->w) ; i++) {
        for (j = 0 ; j < (dst->h) ; j++) {
            put_pixel(dst, i, j,
                v_abs(src->a[i][j].brightness) + v_abs(dst->a[i][j].brightness),
                v_abs(src->a[i][j].color.red) + v_abs(dst->a[i][j].color.red),
                v_abs(src->a[i][j].color.green) + v_abs(dst->a[i][j].color.green),
                v_abs(src->a[i][j].color.blue) + v_abs(dst->a[i][j].color.blue)
            );
        }
    }
    clean_image(dst);
}

/**
 * \fn void put_pixel (t_img *img, int x, int y, int bright, int red, int green, int blue)
 * \brief Fonction insérant les composantes dans un pixel d'une structure t_img
 * @param[in]   x       Entier représentant l'abscisse du pixel
 * @param[in]   y       Entier représentant l'ordonnée du pixel
 * @param[in]   bright  Entier représentant la composante luminosité du pixel
 * @param[in]   red     Entier représentant la composante rouge du pixel
 * @param[in]   green   Entier représentant la composante verte du pixel
 * @param[in]   blue    Entier représentant la composante bleu du pixel
 * @param[out]  img     Pointeur d'un t_img correspondant à l'image
 */

void put_pixel (t_img *img, int x, int y, int bright, int red, int green, int blue) {
    img->a[x][y].brightness = bright;
    img->a[x][y].color.red = red;
    img->a[x][y].color.green = green;
    img->a[x][y].color.blue = blue;
}

/**
 * \fn void show_img (t_img *img)
 * \brief Fonction affichant la structure t_img pour les devs de l'application
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

void show_img (t_img *img) {
    printf("Name : %s\n", img->name);
    printf("Extension : ");
    switch (img->ext) {
        case 0:
            printf("UNKNOWN\n");
        break;
        case PPM:
            printf(".ppm\n");
        break;
        case PGM:
            printf(".pgm\n");
        break;
        case PBM:
            printf(".pbm\n");
        break;
        default:
            send_err("Extension du fichier invalide");
        break;
    }
    printf("Type : ");
    switch (img->type) {
        case 0:
            printf("UNKNOWN\n");
        break;
        case P1:
            printf("P1\n");
        break;
        case P2:
            printf("P2\n");
        break;
        case P3:
            printf("P3\n");
        break;
        case P4:
            printf("P4\n");
        break;
        case P5:
            printf("P5\n");
        break;
        case P6:
            printf("P6\n");
        break;
        default:
            send_err("Type de fichier invalide");
        break;
    }
    printf("Width : %d\n", img->w);
    printf("Height : %d\n", img->h);
    printf("Max : %d\n", img->max);
    printf("Matrix :\n");
    int i = 0, j = 0;
    for (j = 0 ; j < (img->h) ; j++) {
        for (i = 0 ; i < (img->w) ; i++) {
            if ((img->ext == PBM && (img->type == P1 || img->type == P4)) ||
                (img->ext == PGM && (img->type == P2 || img->type == P5)) ||
                (img->ext == 0) ||
                (img->type == 0))
                printf("%d ", img->a[i][j].brightness);
            else
                printf("[R:%d G:%d B:%d] ", img->a[i][j].color.red, img->a[i][j].color.green, img->a[i][j].color.blue);
        }
        printf("\n");
    }
}
