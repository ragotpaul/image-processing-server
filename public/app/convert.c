/**
 * \file convert.c
 * \brief Fonctions de convertion d'une image
 * \version 1.0
 * \date 10/12/2015
 *
 * Contient toutes les fonctions permettant de convertir les images pixmap dans les différents formats.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "config.h"
#include "type.h"
#include "addon.h"
#include "ips.h"

#include "binarisation.h"
#include "negatif.h"
#include "gray.h"

#include "convert.h"

static void pbm_pgm (t_img *img);
static void pbm_ppm (t_img *img);
static void pgm_pbm (t_img *img);
static void pgm_ppm (t_img *img);
static void ppm_pbm (t_img *img);
static void ppm_pgm (t_img *img);

/**
 * \fn static void pbm_pgm (t_img *img)
 * \brief Fonction de conversion d'une image PBM en PGM
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

static void pbm_pgm (t_img *img) {
    if ((img->type == P1 || img->type == P4) && img->ext == PBM) {
        negatif(img);
        img->type++;
        img->ext++;
        img->max = 1;
    } else
        send_err("Format du fichier invalide");
}

/**
 * \fn static void pbm_ppm (t_img *img)
 * \brief Fonction de conversion d'une image PBM en PPM
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

static void pbm_ppm (t_img *img) {
    if ((img->type == P1 || img->type == P4) && img->ext == PBM) {
        pbm_pgm(img);
        pgm_ppm(img);
    } else
        send_err("Format du fichier invalide");
}

/**
 * \fn static void pgm_pbm (t_img *img)
 * \brief Fonction de conversion d'une image PGM en PBM
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

static void pgm_pbm (t_img *img) {
    if ((img->type == P2 || img->type == P5) && img->ext == PGM) {
        binarisation(img);
    } else
        send_err("Format du fichier invalide");
}

/**
 * \fn static void pgm_ppm (t_img *img)
 * \brief Fonction de conversion d'une image PGM en PPM
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

static void pgm_ppm (t_img *img) {
    if ((img->type == P2 || img->type == P5) && img->ext == PGM) {
        int i = 0;
        int j = 0;
        for (j = 0 ; j < (img->h) ; j++) {
            for(i = 0 ; i < (img->w) ; i++) {
                img->a[i][j].color.red = img->a[i][j].brightness;
                img->a[i][j].color.green = img->a[i][j].brightness;
                img->a[i][j].color.blue = img->a[i][j].brightness;
            }
        }
        img->type++;
        img->ext++;
    } else
        send_err("Format du fichier invalide");
}

/**
 * \fn static void ppm_pbm (t_img *img)
 * \brief Fonction de conversion d'une image PPM en PBM
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

static void ppm_pbm (t_img *img) {
    if ((img->type == P3 || img->type == P6) && img->ext == PPM) {
        ppm_pgm(img);
        pgm_pbm(img);
    } else
        send_err("Format du fichier invalide");
}

/**
 * \fn static void ppm_pgm (t_img *img)
 * \brief Fonction de conversion d'une image PPM en PGM
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

static void ppm_pgm (t_img *img) {
    if ((img->type == P3 || img->type == P6) && img->ext == PPM) {
        gray_level_base(img);
    } else
        send_err("Format du fichier invalide");
}

/**
 * \fn void convert (t_img *img, t_magicNumber t)
 * \brief Fonction permettant de convertir n'importe quelle image pixmap dans n'importe quel type
 * @param[in]   t   Nombre entier representant le type voulu grace à la structure t_magicNumber
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

void convert (t_img *img, t_magicNumber t) {
    if ((img->type == P1 || img->type == P4) && img->ext == PBM && (t == P2 || t == P5))
        pbm_pgm(img);
    else if ((img->type == P1 || img->type == P4) && img->ext == PBM && (t == P3 || t == P6))
        pbm_ppm(img);
    else if ((img->type == P2 || img->type == P5) && img->ext == PGM && (t == P1 || t == P4))
        pgm_pbm(img);
    else if ((img->type == P2 || img->type == P5) && img->ext == PGM && (t == P3 || t == P6))
        pgm_ppm(img);
    else if ((img->type == P3 || img->type == P6) && img->ext == PPM && (t == P1 || t == P4))
        ppm_pbm(img);
    else if ((img->type == P3 || img->type == P6) && img->ext == PPM && (t == P2 || t == P5))
        ppm_pgm(img);
    if ((img->type < P4 && t > P3) || (img->type > P3 && t < P4))
        img->type = t;
}
