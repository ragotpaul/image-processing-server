/**
 * \file negatif.c
 * \brief Fonctions de passage au négatif d'une image
 * \version 1.0
 * \date 10/12/2015
 *
 * Contient toutes les fonctions permettant d'obtenir le negatif d'une image.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "config.h"
#include "type.h"
#include "addon.h"
#include "ips.h"

#include "negatif.h"

/**
 * \fn void negatif (t_img *img)
 * \brief Fonction passage au négatif d'une image
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

void negatif (t_img *img) {
    if (img->ext == PBM && (img->type == P1 || img->type == P4)) {
        int i = 0;
        int j = 0;
        for (j = 0 ; j < (img->h) ; j++) {
            for(i = 0 ; i < (img->w) ; i++) {
                img->a[i][j].brightness = (img->a[i][j].brightness) ? 0 : 1;
            }
        }
    } else if (img->ext == PGM && (img->type == P2 || img->type == P5)) {
        int limit = img->max;
        int i = 0;
        int j = 0;
        for (j = 0 ; j < (img->h) ; j++) {
            for(i = 0 ; i < (img->w) ; i++) {
                int bright = img->a[i][j].brightness;
                img->a[i][j].brightness = limit - bright;
            }
        }
    } else if (img->ext == PPM && (img->type == P3 || img->type == P6)) {
        int limit= img->max;
        int i = 0;
        int j = 0;
        for (j = 0 ; j < (img->h) ; j++) {
            for(i = 0 ; i < (img->w) ; i++) {
                int r = img->a[i][j].color.red;
                int g = img->a[i][j].color.green;
                int b = img->a[i][j].color.blue;
                img->a[i][j].color.red = limit - r;
                img->a[i][j].color.green = limit - g;
                img->a[i][j].color.blue = limit - b;
            }
        }
    } else send_err("Format du fichier invalide");
}
