/**
 * \file main.c
 * \brief Fichiers principale
 * \version 1.0
 * \date 10/12/2015
 */

#include <stdio.h>
#include <stdlib.h>
#include "ips.h"

/**
 * \fn int main (int argc, char **argv)
 * \brief Fonction main
 * @param[in]   argc    Taille du tableau argv
 * @param[out]  argv    Tableau contenant les options du programmes
 */

int main (int argc, char **argv) {
    read_arg(argv, argc);
    return 0;
}
