#!/bin/bash -v
mkdir build/
mkdir img/out/

(cd build && cmake .. && make)

./build/ips -b img/cauchy/cauchy.ascii.pbm img/out/cauchy.ascii.pbm-B
./build/ips -b img/cauchy/cauchy.ascii.pgm img/out/cauchy.ascii.pgm-B
./build/ips -b img/cauchy/cauchy.ascii.ppm img/out/cauchy.ascii.ppm-B
./build/ips -b img/cauchy/cauchy.bin.pbm img/out/cauchy.bin.pbm-B
./build/ips -b img/cauchy/cauchy.bin.pgm img/out/cauchy.bin.pgm-B
./build/ips -b img/cauchy/cauchy.bin.ppm img/out/cauchy.bin.ppm-B

./build/ips -n img/cauchy/cauchy.ascii.pbm img/out/cauchy.ascii.pbm-N
./build/ips -n img/cauchy/cauchy.ascii.pgm img/out/cauchy.ascii.pgm-N
./build/ips -n img/cauchy/cauchy.ascii.ppm img/out/cauchy.ascii.ppm-N
./build/ips -n img/cauchy/cauchy.bin.pbm img/out/cauchy.bin.pbm-N
./build/ips -n img/cauchy/cauchy.bin.pgm img/out/cauchy.bin.pgm-N
./build/ips -n img/cauchy/cauchy.bin.ppm img/out/cauchy.bin.ppm-N

./build/ips -g img/cauchy/cauchy.ascii.pbm img/out/cauchy.ascii.pbm-G
./build/ips -g img/cauchy/cauchy.ascii.pgm img/out/cauchy.ascii.pgm-G
./build/ips -g img/cauchy/cauchy.ascii.ppm img/out/cauchy.ascii.ppm-G
./build/ips -g img/cauchy/cauchy.bin.pbm img/out/cauchy.bin.pbm-G
./build/ips -g img/cauchy/cauchy.bin.pgm img/out/cauchy.bin.pgm-G
./build/ips -g img/cauchy/cauchy.bin.ppm img/out/cauchy.bin.ppm-G

./build/ips -c img/cauchy/cauchy.ascii.pbm img/out/cauchy.ascii.pbm-C
./build/ips -c img/cauchy/cauchy.ascii.pgm img/out/cauchy.ascii.pgm-C
./build/ips -c img/cauchy/cauchy.ascii.ppm img/out/cauchy.ascii.ppm-C
./build/ips -c img/cauchy/cauchy.bin.pbm img/out/cauchy.bin.pbm-C
./build/ips -c img/cauchy/cauchy.bin.pgm img/out/cauchy.bin.pgm-C
./build/ips -c img/cauchy/cauchy.bin.ppm img/out/cauchy.bin.ppm-C

./build/ips -h img/cauchy/cauchy.ascii.pbm img/out/cauchy.ascii.pbm-H
./build/ips -h img/cauchy/cauchy.ascii.pgm img/out/cauchy.ascii.pgm-H
./build/ips -h img/cauchy/cauchy.ascii.ppm img/out/cauchy.ascii.ppm-H
./build/ips -h img/cauchy/cauchy.bin.pbm img/out/cauchy.bin.pbm-H
./build/ips -h img/cauchy/cauchy.bin.pgm img/out/cauchy.bin.pgm-H
./build/ips -h img/cauchy/cauchy.bin.ppm img/out/cauchy.bin.ppm-H

./build/ips -v img/cauchy/cauchy.ascii.pbm img/out/cauchy.ascii.pbm-V
./build/ips -v img/cauchy/cauchy.ascii.pgm img/out/cauchy.ascii.pgm-V
./build/ips -v img/cauchy/cauchy.ascii.ppm img/out/cauchy.ascii.ppm-V
./build/ips -v img/cauchy/cauchy.bin.pbm img/out/cauchy.bin.pbm-V
./build/ips -v img/cauchy/cauchy.bin.pgm img/out/cauchy.bin.pgm-V
./build/ips -v img/cauchy/cauchy.bin.ppm img/out/cauchy.bin.ppm-V

./build/ips -C 27 12 19 94 img/cauchy/cauchy.ascii.pbm img/out/cauchy.ascii.pbm-Crop
./build/ips -C 27 12 19 94 img/cauchy/cauchy.ascii.pgm img/out/cauchy.ascii.pgm-Crop
./build/ips -C 27 12 19 94 img/cauchy/cauchy.ascii.ppm img/out/cauchy.ascii.ppm-Crop
./build/ips -C 27 12 19 94 img/cauchy/cauchy.bin.pbm img/out/cauchy.bin.pbm-Crop
./build/ips -C 27 12 19 94 img/cauchy/cauchy.bin.pgm img/out/cauchy.bin.pgm-Crop
./build/ips -C 27 12 19 94 img/cauchy/cauchy.bin.ppm img/out/cauchy.bin.ppm-Crop

./build/ips --convert P1 img/cauchy/cauchy.ascii.pbm img/out/cauchy.ascii.pbm-P1
./build/ips --convert P2 img/cauchy/cauchy.ascii.pbm img/out/cauchy.ascii.pbm-P2
./build/ips --convert P3 img/cauchy/cauchy.ascii.pbm img/out/cauchy.ascii.pbm-P3
./build/ips --convert P4 img/cauchy/cauchy.ascii.pbm img/out/cauchy.ascii.pbm-P4
./build/ips --convert P5 img/cauchy/cauchy.ascii.pbm img/out/cauchy.ascii.pbm-P5
./build/ips --convert P6 img/cauchy/cauchy.ascii.pbm img/out/cauchy.ascii.pbm-P6

./build/ips --convert P1 img/cauchy/cauchy.ascii.pgm img/out/cauchy.ascii.pgm-P1
./build/ips --convert P2 img/cauchy/cauchy.ascii.pgm img/out/cauchy.ascii.pgm-P2
./build/ips --convert P3 img/cauchy/cauchy.ascii.pgm img/out/cauchy.ascii.pgm-P3
./build/ips --convert P4 img/cauchy/cauchy.ascii.pgm img/out/cauchy.ascii.pgm-P4
./build/ips --convert P5 img/cauchy/cauchy.ascii.pgm img/out/cauchy.ascii.pgm-P5
./build/ips --convert P6 img/cauchy/cauchy.ascii.pgm img/out/cauchy.ascii.pgm-P6

./build/ips --convert P1 img/cauchy/cauchy.ascii.ppm img/out/cauchy.ascii.ppm-P1
./build/ips --convert P2 img/cauchy/cauchy.ascii.ppm img/out/cauchy.ascii.ppm-P2
./build/ips --convert P3 img/cauchy/cauchy.ascii.ppm img/out/cauchy.ascii.ppm-P3
./build/ips --convert P4 img/cauchy/cauchy.ascii.ppm img/out/cauchy.ascii.ppm-P4
./build/ips --convert P5 img/cauchy/cauchy.ascii.ppm img/out/cauchy.ascii.ppm-P5
./build/ips --convert P6 img/cauchy/cauchy.ascii.ppm img/out/cauchy.ascii.ppm-P6

./build/ips --convert P1 img/cauchy/cauchy.bin.pbm img/out/cauchy.bin.pbm-P1
./build/ips --convert P2 img/cauchy/cauchy.bin.pbm img/out/cauchy.bin.pbm-P2
./build/ips --convert P3 img/cauchy/cauchy.bin.pbm img/out/cauchy.bin.pbm-P3
./build/ips --convert P4 img/cauchy/cauchy.bin.pbm img/out/cauchy.bin.pbm-P4
./build/ips --convert P5 img/cauchy/cauchy.bin.pbm img/out/cauchy.bin.pbm-P5
./build/ips --convert P6 img/cauchy/cauchy.bin.pbm img/out/cauchy.bin.pbm-P6

./build/ips --convert P1 img/cauchy/cauchy.bin.pgm img/out/cauchy.bin.pgm-P1
./build/ips --convert P2 img/cauchy/cauchy.bin.pgm img/out/cauchy.bin.pgm-P2
./build/ips --convert P3 img/cauchy/cauchy.bin.pgm img/out/cauchy.bin.pgm-P3
./build/ips --convert P4 img/cauchy/cauchy.bin.pgm img/out/cauchy.bin.pgm-P4
./build/ips --convert P5 img/cauchy/cauchy.bin.pgm img/out/cauchy.bin.pgm-P5
./build/ips --convert P6 img/cauchy/cauchy.bin.pgm img/out/cauchy.bin.pgm-P6

./build/ips --convert P1 img/cauchy/cauchy.bin.ppm img/out/cauchy.bin.ppm-P1
./build/ips --convert P2 img/cauchy/cauchy.bin.ppm img/out/cauchy.bin.ppm-P2
./build/ips --convert P3 img/cauchy/cauchy.bin.ppm img/out/cauchy.bin.ppm-P3
./build/ips --convert P4 img/cauchy/cauchy.bin.ppm img/out/cauchy.bin.ppm-P4
./build/ips --convert P5 img/cauchy/cauchy.bin.ppm img/out/cauchy.bin.ppm-P5
./build/ips --convert P6 img/cauchy/cauchy.bin.ppm img/out/cauchy.bin.ppm-P6

./build/ips --smooth img/cauchy/cauchy.ascii.pbm img/out/cauchy.ascii.pbm-Smooth
./build/ips --smooth img/cauchy/cauchy.ascii.pgm img/out/cauchy.ascii.pgm-Smooth
./build/ips --smooth img/cauchy/cauchy.ascii.ppm img/out/cauchy.ascii.ppm-Smooth
./build/ips --smooth img/cauchy/cauchy.bin.pbm img/out/cauchy.bin.pbm-Smooth
./build/ips --smooth img/cauchy/cauchy.bin.pgm img/out/cauchy.bin.pgm-Smooth
./build/ips --smooth img/cauchy/cauchy.bin.ppm img/out/cauchy.bin.ppm-Smooth

./build/ips --gradient-x img/cauchy/cauchy.ascii.pbm img/out/cauchy.ascii.pbm-Gradient-X
./build/ips --gradient-x img/cauchy/cauchy.ascii.pgm img/out/cauchy.ascii.pgm-Gradient-X
./build/ips --gradient-x img/cauchy/cauchy.ascii.ppm img/out/cauchy.ascii.ppm-Gradient-X
./build/ips --gradient-x img/cauchy/cauchy.bin.pbm img/out/cauchy.bin.pbm-Gradient-X
./build/ips --gradient-x img/cauchy/cauchy.bin.pgm img/out/cauchy.bin.pgm-Gradient-X
./build/ips --gradient-x img/cauchy/cauchy.bin.ppm img/out/cauchy.bin.ppm-Gradient-X

./build/ips --gradient-y img/cauchy/cauchy.ascii.pbm img/out/cauchy.ascii.pbm-Gradient-Y
./build/ips --gradient-y img/cauchy/cauchy.ascii.pgm img/out/cauchy.ascii.pgm-Gradient-Y
./build/ips --gradient-y img/cauchy/cauchy.ascii.ppm img/out/cauchy.ascii.ppm-Gradient-Y
./build/ips --gradient-y img/cauchy/cauchy.bin.pbm img/out/cauchy.bin.pbm-Gradient-Y
./build/ips --gradient-y img/cauchy/cauchy.bin.pgm img/out/cauchy.bin.pgm-Gradient-Y
./build/ips --gradient-y img/cauchy/cauchy.bin.ppm img/out/cauchy.bin.ppm-Gradient-Y

./build/ips --gradient img/cauchy/cauchy.ascii.pbm img/out/cauchy.ascii.pbm-Gradient
./build/ips --gradient img/cauchy/cauchy.ascii.pgm img/out/cauchy.ascii.pgm-Gradient
./build/ips --gradient img/cauchy/cauchy.ascii.ppm img/out/cauchy.ascii.ppm-Gradient
./build/ips --gradient img/cauchy/cauchy.bin.pbm img/out/cauchy.bin.pbm-Gradient
./build/ips --gradient img/cauchy/cauchy.bin.pgm img/out/cauchy.bin.pgm-Gradient
./build/ips --gradient img/cauchy/cauchy.bin.ppm img/out/cauchy.bin.ppm-Gradient

./build/ips --sobel-x img/cauchy/cauchy.ascii.pbm img/out/cauchy.ascii.pbm-Sobel-X
./build/ips --sobel-x img/cauchy/cauchy.ascii.pgm img/out/cauchy.ascii.pgm-Sobel-X
./build/ips --sobel-x img/cauchy/cauchy.ascii.ppm img/out/cauchy.ascii.ppm-Sobel-X
./build/ips --sobel-x img/cauchy/cauchy.bin.pbm img/out/cauchy.bin.pbm-Sobel-X
./build/ips --sobel-x img/cauchy/cauchy.bin.pgm img/out/cauchy.bin.pgm-Sobel-X
./build/ips --sobel-x img/cauchy/cauchy.bin.ppm img/out/cauchy.bin.ppm-Sobel-X

./build/ips --sobel-y img/cauchy/cauchy.ascii.pbm img/out/cauchy.ascii.pbm-Sobel-Y
./build/ips --sobel-y img/cauchy/cauchy.ascii.pgm img/out/cauchy.ascii.pgm-Sobel-Y
./build/ips --sobel-y img/cauchy/cauchy.ascii.ppm img/out/cauchy.ascii.ppm-Sobel-Y
./build/ips --sobel-y img/cauchy/cauchy.bin.pbm img/out/cauchy.bin.pbm-Sobel-Y
./build/ips --sobel-y img/cauchy/cauchy.bin.pgm img/out/cauchy.bin.pgm-Sobel-Y
./build/ips --sobel-y img/cauchy/cauchy.bin.ppm img/out/cauchy.bin.ppm-Sobel-Y

./build/ips --sobel img/cauchy/cauchy.ascii.pbm img/out/cauchy.ascii.pbm-Sobel
./build/ips --sobel img/cauchy/cauchy.ascii.pgm img/out/cauchy.ascii.pgm-Sobel
./build/ips --sobel img/cauchy/cauchy.ascii.ppm img/out/cauchy.ascii.ppm-Sobel
./build/ips --sobel img/cauchy/cauchy.bin.pbm img/out/cauchy.bin.pbm-Sobel
./build/ips --sobel img/cauchy/cauchy.bin.pgm img/out/cauchy.bin.pgm-Sobel
./build/ips --sobel img/cauchy/cauchy.bin.ppm img/out/cauchy.bin.ppm-Sobel

./build/ips --laplace img/cauchy/cauchy.ascii.pbm img/out/cauchy.ascii.pbm-Laplace
./build/ips --laplace img/cauchy/cauchy.ascii.pgm img/out/cauchy.ascii.pgm-Laplace
./build/ips --laplace img/cauchy/cauchy.ascii.ppm img/out/cauchy.ascii.ppm-Laplace
./build/ips --laplace img/cauchy/cauchy.bin.pbm img/out/cauchy.bin.pbm-Laplace
./build/ips --laplace img/cauchy/cauchy.bin.pgm img/out/cauchy.bin.pgm-Laplace
./build/ips --laplace img/cauchy/cauchy.bin.ppm img/out/cauchy.bin.ppm-Laplace

./build/ips --edge-sobel img/cauchy/cauchy.ascii.pbm img/out/cauchy.ascii.pbm-Edge-Sobel
./build/ips --edge-sobel img/cauchy/cauchy.ascii.pgm img/out/cauchy.ascii.pgm-Edge-Sobel
./build/ips --edge-sobel img/cauchy/cauchy.ascii.ppm img/out/cauchy.ascii.ppm-Edge-Sobel
./build/ips --edge-sobel img/cauchy/cauchy.bin.pbm img/out/cauchy.bin.pbm-Edge-Sobel
./build/ips --edge-sobel img/cauchy/cauchy.bin.pgm img/out/cauchy.bin.pgm-Edge-Sobel
./build/ips --edge-sobel img/cauchy/cauchy.bin.ppm img/out/cauchy.bin.ppm-Edge-Sobel

./build/ips --edge-laplace img/cauchy/cauchy.ascii.pbm img/out/cauchy.ascii.pbm-Edge-Laplace
./build/ips --edge-laplace img/cauchy/cauchy.ascii.pgm img/out/cauchy.ascii.pgm-Edge-Laplace
./build/ips --edge-laplace img/cauchy/cauchy.ascii.ppm img/out/cauchy.ascii.ppm-Edge-Laplace
./build/ips --edge-laplace img/cauchy/cauchy.bin.pbm img/out/cauchy.bin.pbm-Edge-Laplace
./build/ips --edge-laplace img/cauchy/cauchy.bin.pgm img/out/cauchy.bin.pgm-Edge-Laplace
./build/ips --edge-laplace img/cauchy/cauchy.bin.ppm img/out/cauchy.bin.ppm-Edge-Laplace

./build/ips --draw-edge img/cauchy/cauchy.ascii.pbm img/out/cauchy.ascii.pbm-Draw-Edge
./build/ips --draw-edge img/cauchy/cauchy.ascii.pgm img/out/cauchy.ascii.pgm-Draw-Edge
./build/ips --draw-edge img/cauchy/cauchy.ascii.ppm img/out/cauchy.ascii.ppm-Draw-Edge
./build/ips --draw-edge img/cauchy/cauchy.bin.pbm img/out/cauchy.bin.pbm-Draw-Edge
./build/ips --draw-edge img/cauchy/cauchy.bin.pgm img/out/cauchy.bin.pgm-Draw-Edge
./build/ips --draw-edge img/cauchy/cauchy.bin.ppm img/out/cauchy.bin.ppm-Draw-Edge
