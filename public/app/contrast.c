/**
 * \file contrast.c
 * \brief Fonctions d'amélioration du contraste d'une image
 * \version 1.0
 * \date 10/12/2015
 *
 * Contient toutes les fonctions permettant d'améliorer le contraste d'une image.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "config.h"
#include "type.h"
#include "addon.h"
#include "ips.h"

#include "contrast.h"

/**
 * \fn static int min_img (t_img *img)
 * \brief Fonction permettant de trouver le pixel d'une image dont la luminosité est la plus faible
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

static int min_img (t_img *img) {
    int i = 0;
    int j = 0;
    int tmp = img->a[0][0].brightness;
    for (j=0 ; j<(img->h)-1 ; j++) {
        for(i=0 ; i<(img->w)-1 ; i++) {
                if (img->a[i][j].brightness < tmp) {
                    tmp = img->a[i][j].brightness;
                }
        }
    }
    return tmp;
}

/**
 * \fn static int max_img (t_img *img)
 * \brief Fonction permettant de trouver le pixel d'une image dont la luminosité est la plus forte
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

static int max_img (t_img *img) {
    int i = 0;
    int j = 0;
    int tmp = img->a[0][0].brightness;
    for (j=0 ; j<(img->h)-1 ; j++) {
        for(i=0 ; i<(img->w)-1 ; i++) {
                if (img->a[i][j].brightness > tmp) {
                    tmp = img->a[i][j].brightness;
                }
        }
    }
    return tmp;
}

/**
 * \fn void increase_contrast (t_img *img)
 * \brief Fonction permettant d'améliorer le contraste d'une image
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

void increase_contrast (t_img *img) {
    if (img->ext == PGM && (img->type == P2 || img->type == P5)) {
        int i = 0;
        int j = 0;
        int min = min_img(img);
        int max = max_img(img);
        int a = ((img->max) / (max - min));
        int b = ((img->max * min) / (max - min));
        for (j=0 ; j<(img->h) ; j++) {
            for(i=0 ; i<(img->w) ; i++) {
                img->a[i][j].brightness = a * (img->a[i][j].brightness) + b;
            }
        }
    } else send_err("Format du fichier invalide");
}
