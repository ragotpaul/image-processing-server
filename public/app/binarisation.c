/**
 * \file binarisation.c
 * \brief Fonctions de binarisation par seuillage d'une image
 * \version 1.0
 * \date 10/12/2015
 *
 * Contient les fonctions permettant de binariser une image.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "config.h"
#include "type.h"
#include "addon.h"
#include "ips.h"

#include "gray.h"

#include "binarisation.h"

/**
 * \fn void thresholding (t_img *img, int s)
 * \brief Fonction de binarisation par seuillage d'une image avec une limite défini
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 * @param[in]   s Entier representant la limite du seuillage
 */

void thresholding (t_img *img, int s) {
    if (img->ext == PGM && (img->type == P2 || img->type == P5)) {
        int i = 0;
        int j = 0;
        for (j = 0 ; j < (img->h) ; j++) {
            for(i = 0 ; i < (img->w) ; i++) {
                if (img->a[i][j].brightness > s)
                   img->a[i][j].brightness = 0;
                else
                   img->a[i][j].brightness = 1;
            }
        }
        img->type--;
        img->ext--;
        img->max = 1;
    } else if (img->ext == PPM && (img->type == P3 || img->type == P6)) {
        gray_level_base(img);
        thresholding(img, s);
    }
}

/**
 * \fn void binarisation (t_img *img)
 * \brief Fonction de binarisation par seuillage d'une image avec la moyenne
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

void binarisation (t_img *img) {
    int avg = 0;
    int limit = img->max;
    if (limit % 2 == 1) {
        avg = (limit + 1) / 2;
    } else {
        avg = limit / 2;
    }
    thresholding(img, avg);
}
