#ifndef GRAY_H
#define GRAY_H

void gray_level_base (t_img *img);
void gray_level_advanced (t_img *img);

#endif
