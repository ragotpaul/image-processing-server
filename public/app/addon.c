/**
 * \file addon.c
 * \brief Fonctions additionnelles
 * \version 1.0
 * \date 10/12/2015
 *
 * Contient toutes les fonctions dont on peut se servir sur n'importe quel projet.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "config.h"
#include "type.h"

#include "addon.h"

/**
 * \fn int send_err (char *message)
 * \brief Fonction de cloture du progra mme avec erreur
 * @param[in]   message Le message d'erreur à renvoyer.
 */

int send_err (char *message) {
    fprintf(stderr, "%s\n", message);
    exit(-1);
}

/**
 * \fn void next_line (FILE *f, int n)
 * \brief Fonction déplacant le pointeur d'un fichier sur la ligne suivante.
 * @param[in]   n   Nombre de ligne à sauter
 * @param[out]  f   Fichier à traiter
 */

void next_line (FILE *f, int n) {
    char c = fgetc(f);
    while (n > 0) {
        do {
            c = fgetc(f);
        } while (c != 10);
        n--;
    }
}

/**
 * \fn int power (int n, int e)
 * \brief Fonction puissance
 * @param[in]   n   Nombre entier à élever à la puissance
 * @param[in]   e   Exposant
 */

int power (int n, int e) {
    if (e > 0)
        return power(n, e--) * n;
    else
        return 1;
}

/**
 * \fn int v_abs (int n)
 * \brief Fonction valeur absolue pour les entiers
 * @param[in]   n   Nombre entier
 */

int v_abs (int n) {
    if (n > 0)
        return n;
    else
        return (-n);
}

/**
 * \fn double v_abs_double (double n)
 * \brief Fonction valeur absolue pour les réels
 * @param[in]   n   Nombre réel
 */

double v_abs_double (double n) {
    if (n > 0.)
        return n;
    else
        return (-n);
}

/**
 * \fn int max (int x, int y)
 * \brief Fonction renvoyant l'entier le plus grand
 * @param[in]   x   Nombre entier
 * @param[in]   y   Nombre entier
 */

int max (int x, int y) {
    return (x > y) ? x : y;
}

/**
 * \fn int min (int x, int y)
 * \brief Fonction renvoyant l'entier le plus petit
 * @param[in]   x   Nombre entier
 * @param[in]   y   Nombre entier
 */

int min (int x, int y) {
    return (x < y) ? x : y;
}

/**
 * \fn int sum (int *a, int size)
 * \brief Fonction de la somme des composantes d'un tableau d'entiers
 * @param[in]   size    Taille du tableau
 * @param[out]  a       Tableau d'entiers
 */

int sum (int *a, int size) {
    size--;
    if (size < 0)
        return 0;
    else
        return sum(a, size) + a[size];
}

/**
 * \fn double sum_double (double *a, int size)
 * \brief Fonction de la somme des composantes d'un tableau de réels
 * @param[in]   size    Taille du tableau
 * @param[out]  a       Tableau de réels
 */

double sum_double (double *a, int size) {
    size--;
    if (size < 0)
        return 0.;
    else
        return sum_double(a, size) + a[size];
}

/**
 * \fn int sum_two_array(int **a, int w, int h)
 * \brief Fonction de la somme des composantes d'un tableau d'entiers en deux dimmensions
 * @param[in]   w   Largeur du tableau
 * @param[in]   h   Hauteur du tableau
 * @param[out]  a   Tableau d'entiers en deux dimmensions
 */

int sum_two_array(int **a, int w, int h) {
    h--;
    if (h < 0)
        return 0;
    else
        return sum(a[h], w) + sum_two_array(a, w, h);
}

/**
 * \fn double sum_two_array_double(double **a, int w, int h)
 * \brief Fonction de la somme des composantes d'un tableau de réels en deux dimmensions
 * @param[in]   w   Largeur du tableau
 * @param[in]   h   Hauteur du tableau
 * @param[out]  a   Tableau de réels en deux dimmensions
 */

double sum_two_array_double(double **a, int w, int h) {
    h--;
    if (h < 0)
        return 0.;
    else
        return sum_double(a[h], w) + sum_two_array_double(a, w, h);
}

/**
 * \fn int sum_abs (int *a, int size)
 * \brief Fonction de la somme des valeurs absolue des composantes d'un tableau d'entiers
 * @param[in]   size    Taille du tableau
 * @param[out]  a       Tableau d'entiers
 */

int sum_abs (int *a, int size) {
    size--;
    if (size < 0)
        return 0;
    else
        return sum_abs(a, size) + v_abs(a[size]);
}

/**
 * \fn double sum_abs_double (double *a, int size)
 * \brief Fonction de la somme des valeurs absolue des composantes d'un tableau de réels
 * @param[in]   size    Taille du tableau
 * @param[out]  a       Tableau de réels
 */

double sum_abs_double (double *a, int size) {
    size--;
    if (size < 0)
        return 0.;
    else
        return sum_abs_double(a, size) + v_abs_double(a[size]);
}

/**
 * \fn int sum_two_array_abs (int **a, int w, int h)
 * \brief Fonction de la somme des valeurs absolue des composantes d'un tableau d'entiers en deux dimmensions
 * @param[in]   w   Largeur du tableau
 * @param[in]   h   Hauteur du tableau
 * @param[out]  a   Tableau d'entiers en deux dimmensions
 */

int sum_two_array_abs (int **a, int w, int h) {
    h--;
    if (h < 0)
        return 0;
    else
        return sum_abs(a[h], w) + sum_two_array_abs(a, w, h);
}

/**
 * \fn double sum_two_array_abs_double (double **a, int w, int h)
 * \brief Fonction de la somme des valeurs absolue des composantes d'un tableau de réels en deux dimmensions
 * @param[in]   w   Largeur du tableau
 * @param[in]   h   Hauteur du tableau
 * @param[out]  a   Tableau de réels en deux dimmensions
 */

double sum_two_array_abs_double (double **a, int w, int h) {
    h--;
    if (h < 0)
        return 0.;
    else
        return sum_abs_double(a[h], w) + sum_two_array_abs_double(a, w, h);
}

/**
 * \fn int avg (int *a, int size)
 * \brief Fonction de moyenne d'un tableau d'entiers
 * @param[in]   size    Taille du tableau
 * @param[out]  a       Tableau d'entiers
 */

int avg (int *a, int size) {
    return (sum(a, size) / size);
}

/**
 * \fn int byte_decimal (int *a)
 * \brief Fonction de conversion d'un octect en integer
 * @param[out] a Tableau contenant les bits de l'octect
 */

int byte_decimal (int *a) {
    return (
        128 * a[0] +
        64 * a[1] +
        32 * a[2] +
        16 * a[3] +
        8 * a[4] +
        4 * a[5] +
        2 * a[6] +
        1 * a[7]
    );
}

/**
 * \fn void decimal_byte (int *b, int n)
 * \brief Fonction de conversion d'un integer en octect
 * @param[in]   n   Entier à convertir
 * @param[out]  b   Tableau contenant les bits de l'octect
 */

void decimal_byte (int *b, int n) {
    int i = 0;
    for (i = 0 ; i < 8 ; i++)
        b[i] = 0;
    if (n > 255) {
        b = NULL;
    } else {
        int k = 8;
        while (n > 0) {
            k--;
            b[k] = n % 2;
            n /= 2;
        }
    }
}

/**
 * \fn void show_array_int (int *a, int size)
 * \brief Fonction affichant un tableau d'entiers
 * @param[in]   size    Taille du tableau
 * @param[out]  a       Tableau d'entier
 */

void show_array_int (int *a, int size) {
    int i = 0;
    for (i = 0 ; i < size ; i++) {
        printf("%d |", a[i]);
    }
    printf("\n");
}

/**
 * \fn void show_array_double (double *a, int size)
 * \brief Fonction affichant un tableau de réels
 * @param[in]   size    Taille du tableau
 * @param[out]  a       Tableau de réels
 */

void show_array_double (double *a, int size) {
    int i = 0;
    for (i = 0 ; i < size ; i++) {
        printf("%lf |", a[i]);
    }
    printf("\n");
}
