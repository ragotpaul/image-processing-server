/**
 * \file ips.c
 * \brief Fonctions lectures des variables argc et argv, avec affichage de l'interface d'aide
 * \version 1.0
 * \date 10/12/2015
 *
 * Contient toutes les fonctions permettant lires les flags et d'afficher l'aide.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "config.h"
#include "type.h"
#include "addon.h"

#include "file.h"
#include "img.h"
#include "convert.h"
#include "binarisation.h"
#include "negatif.h"
#include "gray.h"
#include "contrast.h"
#include "symmetry.h"
#include "crop.h"
#include "convolution.h"
#include "edge.h"

#include "ips.h"

/**
 * \fn static void show_help ()
 * \brief Fonction affichant l'aide
 */

static void show_help () {
    printf("Image Processing Server (v%s)\n", VERSION);
    printf("usage : ips [--help]\n");
    printf("        ips [--version]\n");
    printf("        ips [-b|-n|-g|-c|-h|-v] <source> [destination without extension]\n");
    printf("Commands :\n");
    printf("    -b | --binarisation                 : Binarisation\n");
    printf("    -n | --negatif                      : Negative\n");
    printf("    -g | --gray-level                   : Gray level\n");
    printf("    -c | --contrast                     : Contrast increase\n");
    printf("    -h | --symetry-h                    : Symetry horizontal\n");
    printf("    -v | --symetry-v                    : Symetry vertical\n");
    printf("    -t | --thresholding <level>         : Thresholding with level\n");
    printf("    -C | --crop <x1> <y1> <x2> <y2>     : Crop\n");
    printf("    --convert <magic number>            : Convert to ASCII/binary PBM/PGM/PPM\n");
    printf("    --smooth                            : Smooth filter\n");
    printf("    --gradient-x                        : Simple gradient filter horizontal\n");
    printf("    --gradient-y                        : Simple gradient filter vertical\n");
    printf("    --gradient                          : Simple gradient filter\n");
    printf("    --sobel-x                           : Sobel filter horizontal\n");
    printf("    --sobel-y                           : Sobel filter vertical\n");
    printf("    --sobel                             : Sobel filter\n");
    printf("    --laplace                           : Laplacian filter\n");
    printf("    --edge-sobel                        : Edge detect with sobel\n");
    printf("    --edge-laplace                      : Edge detect with laplace\n");
    printf("    --draw-edge                         : Draw edge on image\n");
    exit(0);
}

/**
 * \fn void read_arg (char **a, int size)
 * \brief Fonction de routage executant les bonnes fonctions pour chaque flags séléctionné par l'utilisateur
 * @param[in]   size    Taille du tableau
 * @param[out]  a       Tableau contenant les options du programmes
 */

void read_arg (char **a, int size) {
    t_img *img = init_image();
    char out[255] = "out";
    char src[255] = "";
    switch (size) {
        case 2:
            if (strcmp(a[1], "--help") != 0 && strcmp(a[1], "--version") != 0)
                send_err("Invalid options, please use --help");
            else if (strcmp(a[1], "--help") == 0)
                show_help();
            else if (strcmp(a[1], "--version") == 0) {
                printf("Image Processing Server (v%s)\n", VERSION);
                exit(0);
            }
        break;
        case 3:
            if ((strcmp(a[1], "--convert") == 0) || (strcmp(a[1], "-C") == 0) || (strcmp(a[1], "-t") == 0) || (strcmp(a[1], "--thresholding") == 0))
                send_err("Invalid options, please use --help");
            else
                strcpy(src, a[2]);
        break;
        case 4:
            if (strcmp(a[1], "-C") == 0)
                send_err("Invalid options, please use --help");
            else if (strcmp(a[1], "--convert") == 0 || (strcmp(a[1], "-t") == 0) || (strcmp(a[1], "--thresholding") == 0))
                strcpy(src, a[3]);
            else {
                strcpy(src, a[2]);
                strcpy(out, a[3]);
            }
        break;
        case 5:
            if (strcmp(a[1], "--convert") == 0 || (strcmp(a[1], "-t") == 0) || (strcmp(a[1], "--thresholding") == 0)) {
                strcpy(src, a[3]);
                strcpy(out, a[4]);
            } else
                send_err("Invalid options, please use --help");
        break;
        case 7:
            if (strcmp(a[1], "-C") == 0) {
                strcpy(src, a[6]);
            } else
                send_err("Invalid options, please use --help");
        break;
        case 8:
            if (strcmp(a[1], "-C") == 0) {
                strcpy(src, a[6]);
                strcpy(out, a[7]);
            } else
                send_err("Invalid options, please use --help");
        break;
        default:
            send_err("Invalid options, please use --help");
        break;
    }
    load_img(img, src);
    clean_image(img);
    printf("Image loaded : %s\n", src);
    if (strcmp(a[1], "-b") == 0 || strcmp(a[1], "--binarisation") == 0)
        binarisation(img);
    else if (strcmp(a[1], "-n") == 0 || strcmp(a[1], "--negatif") == 0)
        negatif(img);
    else if (strcmp(a[1], "-g") == 0 || strcmp(a[1], "--gray-level") == 0)
        gray_level_base(img);
    else if (strcmp(a[1], "-c") == 0 || strcmp(a[1], "--contrast") == 0)
        increase_contrast(img);
    else if (strcmp(a[1], "-h") == 0 || strcmp(a[1], "--symetry-h") == 0)
        symmetry_h(img);
    else if (strcmp(a[1], "-v") == 0 || strcmp(a[1], "--symetry-v") == 0)
        symmetry_v(img);
    else if (strcmp(a[1], "-t") == 0 || strcmp(a[1], "--thresholding") == 0)
        thresholding(img, atoi(a[2]));
    else if (strcmp(a[1], "-C") == 0 || strcmp(a[1], "--crop") == 0)
        crop(img, atoi(a[2]), atoi(a[3]), atoi(a[4]), atoi(a[5]));
    else if ((strcmp(a[1], "--convert") == 0) && (strcmp(a[2], "P1") == 0))
        convert(img, P1);
    else if ((strcmp(a[1], "--convert") == 0) && (strcmp(a[2], "P2") == 0))
        convert(img, P2);
    else if ((strcmp(a[1], "--convert") == 0) && (strcmp(a[2], "P3") == 0))
        convert(img, P3);
    else if ((strcmp(a[1], "--convert") == 0) && (strcmp(a[2], "P4") == 0))
        convert(img, P4);
    else if ((strcmp(a[1], "--convert") == 0) && (strcmp(a[2], "P5") == 0))
        convert(img, P5);
    else if ((strcmp(a[1], "--convert") == 0) && (strcmp(a[2], "P6") == 0))
        convert(img, P6);
    else if (strcmp(a[1], "--smooth") == 0)
        smooth(img);
    else if (strcmp(a[1], "--gradient-x") == 0)
        gradient_x(img);
    else if (strcmp(a[1], "--gradient-y") == 0)
        gradient_y(img);
    else if (strcmp(a[1], "--gradient") == 0)
        simple_gradient(img);
    else if (strcmp(a[1], "--sobel-x") == 0)
        sobel_x(img);
    else if (strcmp(a[1], "--sobel-y") == 0)
        sobel_y(img);
    else if (strcmp(a[1], "--sobel") == 0)
        sobel(img);
    else if (strcmp(a[1], "--laplace") == 0)
        laplace(img);
    else if (strcmp(a[1], "--edge-sobel") == 0)
        edge_sobel(img);
    else if (strcmp(a[1], "--edge-laplace") == 0)
        edge_laplace(img);
    else if (strcmp(a[1], "--draw-edge") == 0)
        draw_edge(img);
    else
        send_err("Invalid options, please use --help");
    clean_image(img);
    save_img(img, out);
    printf("Image saved  : ./%s\n", out);
    free_image(img);
}
