/**
 * \file symmetry.c
 * \brief Fonctions de transformation de symmétrie
 * \version 1.0
 * \date 10/12/2015
 *
 * Contient toutes les fonctions permettant d'obtenir la symmétrie d'un image.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "config.h"
#include "type.h"
#include "addon.h"
#include "ips.h"

#include "symmetry.h"

/**
 * \fn void symmetry_h (t_img *img)
 * \brief Fonction de symmétrie horrizontal d'une image
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

void symmetry_h (t_img *img) {
    t_pixel tmp = {{0, 0}, {0, 0, 0}, 0};
    int i = 0;
    int j = 0;
    int k = 0;
    for (j = 0 ; j < (img->h) / 2 ; j++) {
        for (i = 0 ; i < (img->w) ; i++) {
            k = (img->h) - j - 1;
            tmp = img->a[i][j];
            img->a[i][j] = img->a[i][k];
            img->a[i][k] = tmp;
            img->a[i][k].position.y = k;
            img->a[i][j].position.y = j;
        }
    }
}

/**
 * \fn void symmetry_v (t_img *img)
 * \brief Fonction de symmétrie vertical d'une image
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

void symmetry_v (t_img *img) {
    t_pixel tmp = {{0, 0}, {0, 0, 0}, 0};
    int i = 0;
    int j = 0;
    int k = 0;
    for (j = 0 ; j < (img->h) ; j++) {
        for(i = 0 ; i < (img->w) / 2 ; i++) {
            k = (img->w) - i - 1;
            tmp = img->a[i][j];
            img->a[i][j] = img->a[k][j];
            img->a[k][j] = tmp;
            img->a[k][j].position.x = k;
            img->a[i][j].position.x = i;
        }
    }
}
