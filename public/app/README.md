# Projet différencié
## Serveur de traitement d'images

Format d'images compatibles : .ppm, .pgm, .pbm

## Need
CMake >= 3.0

## Install
mkdir build  
cd build/  
cmake ..  
make  

## Usage
usage : ips [--help]  
        ips [--version]  
        ips [-b|-n|-g|-c|-h|-v] <source> [destination without extension]  
Commands :  
    -b | --binarisation                 : Binarisation  
    -n | --negatif                      : Negative  
    -g | --gray-level                   : Gray level  
    -c | --contrast                     : Contrast increase  
    -h | --symetry-h                    : Symetry horizontal  
    -v | --symetry-v                    : Symetry vertical  
    -t | --thresholding <level>         : Thresholding with level  
    -C | --crop <x1> <y1> <x2> <y2>     : Crop  
    --convert <magic number>            : Convert to ASCII/binary PBM/PGM/PPM  
    --smooth                            : Smooth filter  
    --gradient-x                        : Simple gradient filter horizontal  
    --gradient-y                        : Simple gradient filter vertical  
    --gradient                          : Simple gradient filter  
    --sobel-x                           : Sobel filter horizontal  
    --sobel-y                           : Sobel filter vertical  
    --sobel                             : Sobel filter  
    --laplace                           : Laplacian filter  
    --edge-sobel                        : Edge detect with sobel  
    --edge-laplace                      : Edge detect with laplace  
    --draw-edge                         : Draw edge on image  

## Generate documentation
**Need Doxygen**  
cd build/  
cmake ..  
make doc  
*If you want PDF generate with LaTeX*  
cd build/latex/  
make  
