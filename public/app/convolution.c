/**
 * \file convolution.c
 * \brief Fonctions de convolution d'une image
 * \version 1.0
 * \date 10/12/2015
 *
 * Contient toutes les fonctions permettant d'appliquer un masque de convolution sur une image.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "config.h"
#include "type.h"
#include "addon.h"
#include "ips.h"

#include "img.h"

#include "convolution.h"

/**
 * \fn static int b_corner (t_img *img, int x, int y)
 * \brief Fonction permettant de savoir si un pixel est situé sur un coin
 * @param[in]   x Position en abscisse
 * @param[in]   y Position en ordonnée
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

static int b_corner (t_img *img, int x, int y) {
    return ((x == 0 && y == 0) ||
            (x == 0 && y == img->h - 1) ||
            (x == img->w - 1 && y == 0) ||
            (x == img->w - 1 && y == img->h - 1)
            );
}

/**
 * \fn static int b_side (t_img *img, int x, int y)
 * \brief Fonction permettant de savoir si un pixel est situé sur un coté
 * @param[in]   x Position en abscisse
 * @param[in]   y Position en ordonnée
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

static int b_side (t_img *img, int x, int y) {
    return ((x > 0 && x < img->w - 1 && y == 0) ||
            (x == 0 && y > 0 && y < img->h - 1) ||
            (x > 0 && x < img->w - 1 && y == img->h - 1) ||
            (x == img->w - 1 && y > 0 && y < img->h - 1)
            );
}

/**
 * \fn static int b_left_top_corner (t_img *img, int x, int y)
 * \brief Fonction permettant de savoir si un pixel est situé sur le coin supérieur gauche
 * @param[in]   x Position en abscisse
 * @param[in]   y Position en ordonnée
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

static int b_left_top_corner (t_img *img, int x, int y) {
    return (x == 0 && y == 0);
}

/**
 * \fn static int b_right_top_corner (t_img *img, int x, int y)
 * \brief Fonction permettant de savoir si un pixel est situé sur le coin supérieur droit
 * @param[in]   x Position en abscisse
 * @param[in]   y Position en ordonnée
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

static int b_right_top_corner (t_img *img, int x, int y) {
    return (x == img->w - 1 && y == 0);
}

/**
 * \fn static int b_right_bottom_corner (t_img *img, int x, int y)
 * \brief Fonction permettant de savoir si un pixel est situé sur le coin inférieur droit
 * @param[in]   x Position en abscisse
 * @param[in]   y Position en ordonnée
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

static int b_right_bottom_corner (t_img *img, int x, int y) {
    return (x == img->w - 1 && y == img->h - 1);
}

/**
 * \fn static int b_left_bottom_corner (t_img *img, int x, int y)
 * \brief Fonction permettant de savoir si un pixel est situé sur le coin inférieur gauche
 * @param[in]   x Position en abscisse
 * @param[in]   y Position en ordonnée
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

static int b_left_bottom_corner (t_img *img, int x, int y) {
    return (x == 0 && y == img->h - 1);
}

/**
 * \fn static int b_left_side (t_img *img, int x, int y)
 * \brief Fonction permettant de savoir si un pixel est situé sur le coté gauche
 * @param[in]   x Position en abscisse
 * @param[in]   y Position en ordonnée
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

static int b_left_side (t_img *img, int x, int y) {
    return (x == 0 && y > 0 && y < img->h - 1);
}

/**
 * \fn static int b_top_side (t_img *img, int x, int y)
 * \brief Fonction permettant de savoir si un pixel est situé sur le coté supérieur
 * @param[in]   x Position en abscisse
 * @param[in]   y Position en ordonnée
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

static int b_top_side (t_img *img, int x, int y) {
    return (x > 0 && x < img->w - 1 && y == 0);
}

/**
 * \fn static int b_right_side (t_img *img, int x, int y)
 * \brief Fonction permettant de savoir si un pixel est situé sur le coté droit
 * @param[in]   x Position en abscisse
 * @param[in]   y Position en ordonnée
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

static int b_right_side (t_img *img, int x, int y) {
    return (x == img->w - 1 && y > 0 && y < img->h - 1);
}

/**
 * \fn static int b_bottom_side (t_img *img, int x, int y)
 * \brief Fonction permettant de savoir si un pixel est situé sur le coté inférieur
 * @param[in]   x Position en abscisse
 * @param[in]   y Position en ordonnée
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

static int b_bottom_side (t_img *img, int x, int y) {
    return (x > 0 && x < img->w - 1 && y == img->h - 1);
}

/**
 * \fn void filter (t_img *img, double *kernel, int kernel_switch, double divisor, double offset)
 * \brief Fonction permettant d'appliquer un masque de convolution (kernel) sur une image
 * @param[in]   kernel_switch   Entier permettant de parcourir le kernel (1 pour les kernel 3*3, 2 pour 5*5, 3 pour 6*6 etc ...)
 * @param[in]   divisor         Entier divisant toute les valeur du kernel, c'est le coefficiant du masque
 * @param[in]   offset          Entier s'ajoutant à la composante du pixel une fois la convolution appliqué
 * @param[out]  img             Pointeur d'un t_img correspondant à l'image
 * @param[out]  kernel          Pointeur d'un double correspondant au masque de convolution
 */

void filter (t_img *img, double *kernel, int kernel_switch, double divisor, double offset) {
    t_img *tmp = init_image();
    copy_image(img, tmp);
    int i_x = 0, i_y = 0, j_x = 0, j_y = 0, k = 0, l = 0;
    int c[4] = {0}, s[6] = {0};
    double pixels[4] = {0.};
    for (i_x = 0 ; i_x < (tmp->w) ; i_x++) {
        for (i_y = 0 ; i_y < (tmp->h) ; i_y++) {
            pixels[0] = pixels[1] = pixels[2] = pixels[3] = 0.;
            if (b_corner(img, i_x, i_y)) {
                if (b_left_top_corner(img, i_x, i_y)) {
                    c[0] = 4; c[1] = 5; c[2] = 7; c[3] = 8;
                } else if (b_right_top_corner(img, i_x, i_y)) {
                    c[0] = 3; c[1] = 4; c[2] = 6; c[3] = 7;
                } else if (b_right_bottom_corner(img, i_x, i_y)) {
                    c[0] = 0; c[1] = 1; c[2] = 3; c[3] = 4;
                } else if (b_left_bottom_corner(img, i_x, i_y)) {
                    c[0] = 1; c[1] = 2; c[2] = 4; c[3] = 5;
                }
                k = 0;
                for (l = 0 ; l < 4 ; l++)
                    k += kernel[c[l]];
                pixels[0] = (k / divisor) * ((double) tmp->a[i_x + j_x][i_y + j_y].brightness);
                pixels[1] = (k / divisor) * ((double) tmp->a[i_x + j_x][i_y + j_y].color.red);
                pixels[2] = (k / divisor) * ((double) tmp->a[i_x + j_x][i_y + j_y].color.green);
                pixels[3] = (k / divisor) * ((double) tmp->a[i_x + j_x][i_y + j_y].color.blue);
            } else if (b_side(img, i_x, i_y)) {
                if (b_left_side(img, i_x, i_y)) {
                    s[0] = 1; s[1] = 2; s[2] = 4; s[3] = 5; s[4] = 7; s[5] = 8;
                } else if (b_top_side(img, i_x, i_y)) {
                    s[0] = 3; s[1] = 4; s[2] = 5; s[3] = 6; s[4] = 7; s[5] = 8;
                } else if (b_right_side(img, i_x, i_y)) {
                    s[0] = 0; s[1] = 1; s[2] = 3; s[3] = 4; s[4] = 6; s[5] = 7;
                } else if (b_bottom_side(img, i_x, i_y)) {
                    s[0] = 0; s[1] = 1; s[2] = 2; s[3] = 3; s[4] = 4; s[5] = 5;
                }
                k = 0;
                for (l = 0 ; l < 6 ; l++)
                    k += kernel[s[l]];
                pixels[0] = (k / divisor) * ((double) tmp->a[i_x + j_x][i_y + j_y].brightness);
                pixels[1] = (k / divisor) * ((double) tmp->a[i_x + j_x][i_y + j_y].color.red);
                pixels[2] = (k / divisor) * ((double) tmp->a[i_x + j_x][i_y + j_y].color.green);
                pixels[3] = (k / divisor) * ((double) tmp->a[i_x + j_x][i_y + j_y].color.blue);
            } else {
                for (j_x = -kernel_switch ; j_x <= kernel_switch ; j_x++) {
                    for (j_y = -kernel_switch ; j_y <= kernel_switch ; j_y++) {
                        pixels[0] += (kernel[(j_x + kernel_switch) +
                                        (j_y + kernel_switch) *
                                        (2 * kernel_switch + 1)] / divisor) *
                                        ((double) tmp->a[i_x + j_x][i_y + j_y].brightness);
                        pixels[1] += (kernel[(j_x + kernel_switch) +
                                        (j_y + kernel_switch) *
                                        (2 * kernel_switch + 1)] / divisor) *
                                        ((double) tmp->a[i_x + j_x][i_y + j_y].color.red);
                        pixels[2] += (kernel[(j_x + kernel_switch) +
                                        (j_y + kernel_switch) *
                                        (2 * kernel_switch + 1)] / divisor) *
                                        ((double) tmp->a[i_x + j_x][i_y + j_y].color.green);
                        pixels[3] += (kernel[(j_x + kernel_switch) +
                                        (j_y + kernel_switch) *
                                        (2 * kernel_switch + 1)] / divisor) *
                                        ((double) tmp->a[i_x + j_x][i_y + j_y].color.blue);
                    }
                }
            }
            put_pixel(img, i_x, i_y,
                (int) pixels[0],
                (int) pixels[1],
                (int) pixels[2],
                (int) pixels[3]
            );
        }
    }
    free_image(tmp);
}

/**
 * \fn void smooth(t_img *img)
 * \brief Fonction permettant l'application du masque de lissage d'un image par convolution
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

void smooth(t_img *img) {
    double kernel[9] = {
        1., 1., 1.,
        1., 1., 1.,
        1., 1., 1.
    };
    filter(img, kernel, 1, 9., 0.);
}

/**
 * \fn void gradient_x (t_img *img)
 * \brief Fonction permettant l'application du masque de gradient selon l'axe des abscisses d'un image par convolution
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

void gradient_x (t_img *img) {
    double kernel_x[9] = {
        0., 0., 0.,
        0., -1., 1.,
        0., 0., 0.
    };
    filter(img, kernel_x, 1, 1., 0.);
}

/**
 * \fn void gradient_y (t_img *img)
 * \brief Fonction permettant l'application du masque de gradient selon l'axe des ordonnées d'un image par convolution
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

void gradient_y (t_img *img) {
    double kernel_y[9] = {
        0., 0., 0.,
        0., -1., 0.,
        0., 1., 0.
    };
    filter(img, kernel_y, 1, 1., 0.);
}

/**
 * \fn void simple_gradient (t_img *img)
 * \brief Fonction permettant l'application du masque de gradient selon l'axe des abscisses et des ordonnées d'un image par convolution
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

void simple_gradient (t_img *img) {
    t_img *img_1 = init_image();
    copy_image(img, img_1);
    t_img *img_2 = init_image();
    copy_image(img, img_2);
    gradient_x(img_1);
    gradient_y(img_2);
    merge_image(img_1, img_2);
    copy_image(img_2, img);
    free_image(img_1);
    free_image(img_2);
}

/**
 * \fn void sobel_x (t_img *img)
 * \brief Fonction permettant l'application du masque de sobel selon l'axe des abscisses d'un image par convolution
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

void sobel_x (t_img *img) {
    double kernel_x[9] = {
        -1., 0., 1.,
        -2., 0., 2.,
        -1., 0., 1.
    };
    filter(img, kernel_x, 1, 4., 0.);
}

/**
 * \fn void sobel_y (t_img *img)
 * \brief Fonction permettant l'application du masque de sobel selon l'axe des ordonnées d'un image par convolution
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

void sobel_y (t_img *img) {
    double kernel_y[9] = {
        -1., -2., -1.,
        0., 0., 0.,
        1., 2., 1.
    };
    filter(img, kernel_y, 1, 4., 0.);
}

/**
 * \fn void sobel (t_img *img)
 * \brief Fonction permettant l'application du masque de sobel selon l'axe des abscisses et des ordonnées d'un image par convolution
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

void sobel (t_img *img) {
    t_img *img_1 = init_image();
    copy_image(img, img_1);
    t_img *img_2 = init_image();
    copy_image(img, img_2);
    sobel_x(img_1);
    sobel_y(img_2);
    merge_image(img_1, img_2);
    copy_image(img_2, img);
    free_image(img_1);
    free_image(img_2);
}

/**
 * \fn void laplace (t_img *img)
 * \brief Fonction permettant l'application du masque de laplace d'un image par convolution
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

void laplace (t_img *img) {
    double kernel[9] = {
        0., 1., 0.,
        1., -4., 1.,
        0., 1., 0.
    };
    filter(img, kernel, 1, 1., 0.);
}
