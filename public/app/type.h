#ifndef TYPE_H
#define TYPE_H

/**
 * \struct t_pos
 * \brief Coordonnées
 *
 * Il s'agit de deux integer permettant de stocker des coordonnées.
 *
 */

typedef struct {
    int x;  /*!< Abscisses */
    int y;  /*!< Ordonnées */
} t_pos;

/**
 * \struct t_color
 * \brief Composantes RGB
 *
 * Il s'agit de trois integer permettant de stocker une couleur.
 *
 */

typedef struct {
    int red;    /*!< Rouge */
    int green;  /*!< Vert */
    int blue;   /*!< Bleu */
} t_color;

/**
 * \struct t_pixel
 * \brief Pixels
 *
 * Il s'agit d'un t_pos, d'un t_color et d'un integer pour stocker la luminosité.
 *
 */

typedef struct {
    t_pos position; /*!< Coordonnées */
    t_color color;  /*!< Couleurs */
    int brightness; /*!< Luminosité */
} t_pixel;

/**
 * \struct t_magicNumber
 * \brief Nombre magic du fichier pixmap
 *
 * Il s'agit d'une énumération d'integer correspondant aux nombre magique des fichiers pixmap.
 *
 */

typedef enum {
    P1 = 1, /*!< Fichiers noir et blanc en ASCII */
    P2, /*!< Fichiers niveaux de gris en ASCII */
    P3, /*!< Fichiers couleurs en ASCII */
    P4, /*!< Fichiers noir et blanc en binaire */
    P5, /*!< Fichiers niveaux de gris en binaire */
    P6 /*!< Fichiers couleurs en binaire */
} t_magicNumber;

/**
 * \struct t_extension
 * \brief Extension des fichiers pixmap
 *
 * Il s'agit d'une énumération d'integer correspondant aux extensions des fichiers pixmap.
 *
 */

typedef enum {
    PBM = 1, /*!< Portable Bitmap File Format */
    PGM, /*!< Portable Graymap File Format */
    PPM /*!< Portable Pixmap File Format */
} t_extension;

/**
 * \struct t_img
 * \brief Image
 *
 * Il s'agit de toutes les composantes d'une image pixmap chargée en mémoire.
 *
 */

typedef struct {
    char *name; /*!< String contenant le nom du fichier */
    t_extension ext; /*!< L'extension du fichier */
    t_magicNumber type; /*!< Type de l'image */
    int w;  /*!< Largeur de l'image */
    int h; /*!< Hauteur de l'image */
    int max; /*!< Valeur maximum des pixels de l'image */
    t_pixel **a; /*!< Ensemble des pixels de l'image */
} t_img;

#endif
