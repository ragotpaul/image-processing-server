#ifndef CONFIG_H
#define CONFIG_H

#define MAX_WIDTH 2048
#define MAX_HEIGHT 2048
#define N_PIXELS (MAX_WIDTH * MAX_HEIGHT)
#define PIX_PER_LINE 8
#define VERSION "0.5"

#endif
