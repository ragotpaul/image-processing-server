/**
 * \file edge.c
 * \brief Fonctions de détéction des contours d'une image
 * \version 1.0
 * \date 10/12/2015
 *
 * Contient les fonctions permettant d'éxecuter differentes opérations dans un certain ordre pour détécter les contours d'une image.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "config.h"
#include "type.h"
#include "addon.h"
#include "ips.h"

#include "convolution.h"
#include "negatif.h"
#include "binarisation.h"
#include "gray.h"
#include "convert.h"
#include "img.h"

#include "edge.h"

/**
 * \fn static void convert_edge (t_img *img)
 * \brief Fonction permettant de convertir les bord de l'image pour qu'il soit noir sur blanc
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

static void convert_edge (t_img *img) {
    negatif(img);
    int i = 0;
    int j = 0;
    int avg = 0;
    for (j = 0 ; j < (img->h) ; j++) {
        for(i = 0 ; i < (img->w) ; i++) {
            avg += img->a[i][j].brightness;
        }
    }
    avg /= img->h * img->w;
    thresholding(img, avg);
}

/**
 * \fn void edge_sobel (t_img *img)
 * \brief Fonction permettant de mettre les contours en evidence avec le filtre de convolution de sobel
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

void edge_sobel (t_img *img) {
    if (img->ext == PBM && (img->type == P1 || img->type == P4))
        convert(img, img->type + 2);
    if (img->ext == PPM && (img->type == P3 || img->type == P6))
        gray_level_base(img);
    sobel(img);
    convert_edge(img);
}

/**
 * \fn void edge_laplace (t_img *img)
 * \brief Fonction permettant de mettre les contours en evidence avec le filtre de convolution de laplace
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

void edge_laplace (t_img *img) {
    if (img->ext == PBM && (img->type == P1 || img->type == P4))
        convert(img, img->type + 2);
    smooth(img);
    if (img->ext == PPM && (img->type == P3 || img->type == P6))
        gray_level_base(img);
    t_img *img_gradient = init_image();
    copy_image(img, img_gradient);
    laplace(img);
    simple_gradient(img_gradient);
    merge_image(img_gradient, img);
    convert_edge(img);
    free_image(img_gradient);
}

/**
 * \fn void draw_edge (t_img *img)
 * \brief Fonction permettant supperposer les contours d'une image sur l'image d'origine formant ainsi un dessin
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

 void draw_edge (t_img *img) {
     t_img *img_edge = init_image();
     copy_image(img, img_edge);
     edge_sobel(img_edge);
     int i = 0;
     int j = 0;
     for (j = 0 ; j < (img->h) ; j++) {
         for(i = 0 ; i < (img->w) ; i++) {
             if (img_edge->a[i][j].brightness != 0)
                 put_pixel(img, i, j, 1, 0, 0, 0);
         }
     }
     free_image(img_edge);
 }
