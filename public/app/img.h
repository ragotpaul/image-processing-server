#ifndef IMG_H
#define IMG_H

t_img *init_image ();
void free_image (t_img *img);
void copy_image (t_img *src, t_img *dst);
void erase_pixels (t_img *img);
void clean_image (t_img *img);
void merge_image (t_img *src, t_img *dst);
void put_pixel (t_img *img, int x, int y, int bright, int red, int green, int blue);
void show_img (t_img *img);

#endif
