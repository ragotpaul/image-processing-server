/**
 * \file file.c
 * \brief Fonctions de gestion des fichiers pixmap
 * \version 1.0
 * \date 10/12/2015
 *
 * Contient toutes les fonctions permettant de charger en mémoire les fichiers pixmap, de les sauvegarder et de les afficher pour le devellopement.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "config.h"
#include "type.h"
#include "addon.h"
#include "ips.h"

#include "file.h"

static int binary_file (char *name);
static void load_name (t_img *img, char *name);
static void load_extension (t_img *img, char *name);
static void load_type (t_img *img, FILE *f);
static void load_dimension (t_img *img, FILE *f);
static void load_max (t_img *img, FILE *f);
static void load_pixels (t_img *img, FILE *f);
static void write_pixels (t_img *img, char *name);

/**
 * \fn static int binary_file (char *name)
 * \brief Fonction permettant de savoir si un fichier image pixmap est écrit en binaire ou non
 * @param[out]  name    String du nom du fichier
 */

static int binary_file (char *name) {
    FILE *f = NULL;
    f = fopen(name, "r");
    if (f == NULL)
        send_err("Ouverture du fichier impossible");
    fseek(f, 1, SEEK_SET);
    char n = fgetc(f);
    fclose(f);
    if (n > 48 && n < 52)
        return 0;
    else if (n > 51 && n < 55)
        return 1;
    else
        return -1;
}

/**
 * \fn static void load_name (t_img *img, char *name)
 * \brief Fonction permettant de charger le nom d'un fichier pixmap dans la structure t_img
 * @param[out]  img     Pointeur d'un t_img correspondant à l'image
 * @param[out]  name    String du nom du fichier
 */

static void load_name (t_img *img, char *name) {
    img->name = name;
}

/**
 * \fn static void load_extension (t_img *img, char *name)
 * \brief Fonction permettant de charger l'extension d'un fichier pixmap dans la structure t_img
 * @param[out]  img     Pointeur d'un t_img correspondant à l'image
 * @param[out]  name    String du nom du fichier
 */

static void load_extension (t_img *img, char *name) {
    int l = strlen(name);
    int i = 0, j = 3;
    char ext[4];
    for (i = 0 ; i < 4 ; i++) {
        ext[i] = (i != 3) ? name[l-j] : '\0';
        j--;
    }
    if (strcmp(ext, "ppm") == 0 || strcmp(ext, "PPM") == 0)
        img->ext = PPM;
    else if (strcmp(ext, "pgm") == 0 || strcmp(ext, "PGM") == 0)
        img->ext = PGM;
    else if (strcmp(ext, "pbm") == 0 || strcmp(ext, "PBM") == 0)
        img->ext = PBM;
    else
        send_err("Extension du fichier invalide");
}

/**
 * \fn static void load_type (t_img *img, FILE *f)
 * \brief Fonction permettant de charger le type d'un fichier pixmap dans la structure t_img
 * @param[out]  img     Pointeur d'un t_img correspondant à l'image
 * @param[out]  f       Pointeur vers le fichier chargé en mémoire
 */

static void load_type (t_img *img, FILE *f) {
    rewind(f);
    char c1 = fgetc(f);
    char c2 = fgetc(f);
    if (c1 == 'P') {
        switch(c2) {
            case 49:
                img->type = P1;
            break;
            case 50:
                img->type = P2;
            break;
            case 51:
                img->type = P3;
            break;
            case 52:
                img->type = P4;
            break;
            case 53:
                img->type = P5;
            break;
            case 54:
                img->type = P6;
            break;
            default:
                send_err("Type de fichier invalide");
            break;
        }
    } else
        send_err("Type de fichier invalide");
}

/**
 * \fn static void load_dimension (t_img *img, FILE *f)
 * \brief Fonction permettant de charger les dimensions d'un fichier pixmap dans la structure t_img
 * @param[out]  img     Pointeur d'un t_img correspondant à l'image
 * @param[out]  f       Pointeur vers le fichier chargé en mémoire
 */

static void load_dimension (t_img *img, FILE *f) {
    rewind(f);
    next_line(f, 1);
    int w = 0;
    int h = 0;
    fscanf(f, "%d %d", &w, &h);
    if (w > MAX_WIDTH || h > MAX_HEIGHT)
        send_err("Out of memory");
    img->w = w;
    img->h = h;
}

/**
 * \fn static void load_max (t_img *img, FILE *f)
 * \brief Fonction permettant de charger la valeur maximum des composantes d'un fichier pixmap dans la structure t_img
 * @param[out]  img     Pointeur d'un t_img correspondant à l'image
 * @param[out]  f       Pointeur vers le fichier chargé en mémoire
 */

static void load_max (t_img *img, FILE *f) {
    rewind(f);
    int max = 1;
    if (((img->ext == PGM  && (img->type == P2 || img->type == P5)) || (img->ext == PPM  && (img->type == P3 || img->type == P6)))) {
        next_line(f, 2);
        fscanf(f, "%d", &max);
    }
    img->max = max;
}

/**
 * \fn static void load_pixels (t_img *img, FILE *f)
 * \brief Fonction permettant de charger l'ensemble des pixels d'un fichier pixmap dans la structure t_img
 * @param[out]  img     Pointeur d'un t_img correspondant à l'image
 * @param[out]  f       Pointeur vers le fichier chargé en mémoire
 */

static void load_pixels (t_img *img, FILE *f) {
    rewind(f);
    int i = 0;
    int j = 0;
    if (img->ext == PBM && img->type == P1) { // Image PBM noir et blanc en ASCII
        next_line(f, 2);
        for (j = 0 ; j < (img->h) ; j++) {
            for (i = 0 ; i < (img->w) ; i++) {
                img->a[i][j].position.x = i;
                img->a[i][j].position.y = j;
                img->a[i][j].color.red = 0;
                img->a[i][j].color.green = 0;
                img->a[i][j].color.blue = 0;
                fscanf(f, "%d ", &(img->a[i][j].brightness));
            }
        }
    } else if (img->ext == PGM && img->type == P2) { // Image niveau de gris en ASCII
        next_line(f, 3);
        for (j = 0 ; j < (img->h) ; j++) {
            for (i = 0 ; i < (img->w) ; i++) {
                img->a[i][j].position.x = i;
                img->a[i][j].position.y = j;
                img->a[i][j].color.red = 0;
                img->a[i][j].color.green = 0;
                img->a[i][j].color.blue = 0;
                fscanf(f, "%d ", &(img->a[i][j].brightness));
            }
        }
    } else if (img->ext == PPM && img->type == P3) { // Image couleur en ASCII
        next_line(f, 3);
        for (j = 0 ; j < (img->h) ; j++) {
            for (i = 0 ; i < (img->w) ; i++) {
                img->a[i][j].position.x = i;
                img->a[i][j].position.y = j;
                fscanf(f, "%d %d %d ", &(img->a)[i][j].color.red, &(img->a)[i][j].color.green, &(img->a)[i][j].color.blue);
                img->a[i][j].brightness = 1;
            }
        }
    } else if (img->ext == PBM && img->type == P4) { // Image noir et blanc en binaire
        next_line(f, 2);
        int b[8] = {0};
        int k = 8, n = 0;
        for (j = 0 ; j < (img->h) ; j++) {
            for (i = 0 ; i < (img->w) ; i++) {
                img->a[i][j].position.x = i;
                img->a[i][j].position.y = j;
                img->a[i][j].color.red = 0;
                img->a[i][j].color.green = 0;
                img->a[i][j].color.blue = 0;
                if (k > 7) {
                    k = 0;
                    n = fgetc(f);
                    decimal_byte(b, n);
                }
                img->a[i][j].brightness = b[k++];
            }
            k = 8;
        }
    } else if (img->ext == PGM && img->type == P5) { // Image niveau de gris en binaire
        next_line(f, 3);
        for (j = 0 ; j < (img->h) ; j++) {
            for (i = 0 ; i < (img->w) ; i++) {
                img->a[i][j].position.x = i;
                img->a[i][j].position.y = j;
                img->a[i][j].color.red = 0;
                img->a[i][j].color.green = 0;
                img->a[i][j].color.blue = 0;
                img->a[i][j].brightness = fgetc(f);
            }
        }
    } else if (img->ext == PPM && img->type == P6) { //Image couleur en binaire
        next_line(f, 3);
        for (j = 0 ; j < (img->h) ; j++) {
            for (i = 0 ; i < (img->w) ; i++) {
                img->a[i][j].position.x = i;
                img->a[i][j].position.y = j;
                img->a[i][j].color.red = fgetc(f);
                img->a[i][j].color.green = fgetc(f);
                img->a[i][j].color.blue = fgetc(f);
                img->a[i][j].brightness = 1;
            }
        }
    } else {
        send_err("Fichier image corrompu");
    }
}

/**
 * \fn void load_img (t_img *img, char *name)
 * \brief Fonction permettant de charger un fichier pixmap dans la structure t_img
 * @param[out]  img     Pointeur d'un t_img correspondant à l'image
 * @param[out]  name    String du nom du fichier
 */

void load_img (t_img *img, char *name) {
    int bin = binary_file(name);
    if (bin < 0)
        send_err("Ouverture du fichier impossible");
    FILE *f = NULL;
    if (bin)
        f = fopen(name, "rb");
    else
        f = fopen(name, "r");
    if (f == NULL)
        send_err("Ouverture du fichier impossible");
    load_name(img, name);
    load_extension(img, name);
    load_type(img, f);
    load_dimension(img, f);
    load_max(img, f);
    load_pixels(img, f);
    fclose(f);
}

/**
 * \fn static void write_pixels (t_img *img, char *name)
 * \brief Fonction permettant de sauvegarder les pixels d'une structure t_img dans un fichier pixmap
 * @param[out]  img     Pointeur d'un t_img correspondant à l'image
 * @param[out]  name    String du nom du fichier
 */

static void write_pixels (t_img *img, char *name) {
    FILE *f = NULL;
    if (img->type == P1 || img->type == P2 || img->type == P3) {
        f = fopen(name, "a");
        int i = 0, j = 0, k = 1;
        for (j = 0 ; j < (img->h) ; j++) {
            for (i = 0 ; i < (img->w) ; i++) {
                if ((img->ext == PBM && img->type == P1) || (img->ext == PGM && img->type == P2)) {
                    fprintf(f, "%d ", img->a[i][j].brightness);
                    k++;
                }
                else if (img->ext == PPM && img->type == P3) {
                    fprintf(f, "%d %d %d ", img->a[i][j].color.red, img->a[i][j].color.green, img->a[i][j].color.blue);
                    k += 3;
                } else
                    send_err("Fichier corrompu");
                if (k > PIX_PER_LINE) {
                    fputs("\n", f);
                    k = 1;
                }
            }
            fputs("\n", f);
            k = 1;
        }
        fclose(f);
    } else if (img->type == P4) {
        f = fopen(name, "ab");
        int i = 0, j = 0, k = 0;
        int b[8] = {0};
        for (j = 0 ; j < (img->h) ; j++) {
            for (i = 0 ; i < (img->w) ; i++) {
                b[k++] = img->a[i][j].brightness;
                if (k > 7) {
                    fputc(byte_decimal(b), f);
                    for (k = 0 ; k < 8 ; k++) {
                        b[k] = 0;
                    }
                    k = 0;
                }
            }
            if (k != 0) {
                fputc(byte_decimal(b), f);
                for (k = 0 ; k < 8 ; k++) {
                    b[k] = 0;
                }
                k = 0;
            }
        }
        fclose(f);
    } else if (img->type == P5 || img->type == P6) {
        f = fopen(name, "ab");
        int i = 0, j = 0;
        for (j = 0 ; j < (img->h) ; j++) {
            for (i = 0 ; i < (img->w) ; i++) {
                if (img->ext == PGM && img->type == P5) {
                    fputc(img->a[i][j].brightness, f);
                }
                else if (img->ext == PPM && img->type == P6) {
                    fputc(img->a[i][j].color.red, f);
                    fputc(img->a[i][j].color.green, f);
                    fputc(img->a[i][j].color.blue, f);
                } else
                    send_err("Fichier corrompu");
            }
        }
        fclose(f);
    } else
        send_err("Type de fichier invalide");
    if (f == NULL) send_err("Ouverture du fichier impossible");
}

/**
 * \fn void save_img (t_img *img, char *name)
 * \brief Fonction permettant de sauvegarder le contenu d'une structure t_img dans un fichier pixmap
 * @param[out]  img     Pointeur d'un t_img correspondant à l'image
 * @param[out]  name    String du nom du fichier
 */

void save_img (t_img *img, char *name) {
    FILE *f = NULL;
    switch (img->ext) {
        case PPM:
            strcat(name, ".ppm");
        break;
        case PGM:
            strcat(name, ".pgm");
        break;
        case PBM:
            strcat(name, ".pbm");
        break;
        default:
            send_err("Extension du fichier invalide");
        break;
    }
    f = fopen(name, "w");
    if (f == NULL) send_err("Ouverture du fichier impossible");
    switch (img->type) {
        case P1:
            fputs("P1\n", f);
        break;
        case P2:
            fputs("P2\n", f);
        break;
        case P3:
            fputs("P3\n", f);
        break;
        case P4:
            fputs("P4\n", f);
        break;
        case P5:
            fputs("P5\n", f);
        break;
        case P6:
            fputs("P6\n", f);
        break;
        default:
            send_err("Type de fichier invalide");
        break;
    }
    fprintf(f, "%d %d\n", img->w, img->h);
    if ((img->ext == PGM && (img->type == P2 || img->type == P5)) || (img->ext == PPM && (img->type == P3 || img->type == P6)))
        fprintf(f, "%d\n", img->max);
    fclose(f);
    write_pixels(img, name);
}
