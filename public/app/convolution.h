#ifndef CONVOLUTION_H
#define CONVOLUTION_H

void filter (t_img *img, double *kernel, int kernel_switch, double divisor, double offset);
void smooth(t_img *img);
void gradient_x (t_img *img);
void gradient_y (t_img *img);
void simple_gradient (t_img *img);
void sobel_x (t_img *img);
void sobel_y (t_img *img);
void sobel (t_img *img);
void laplace (t_img *img);

#endif
