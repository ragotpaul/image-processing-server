/**
 * \file gray.c
 * \brief Fonctions passage au niveaux de gris.
 * \version 1.0
 * \date 10/12/2015
 *
 * Contient toutes les fonctions permettant de transformer une image en niveau de gris.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "config.h"
#include "type.h"
#include "addon.h"
#include "ips.h"

#include "gray.h"

/**
 * \fn void gray_level_base (t_img *img)
 * \brief Fonction permettant la transformation d'un image couleur en niveaux de gris
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

void gray_level_base (t_img *img) {
    if (img->ext == PPM && (img->type == P3 || img->type == P6)) {
        int i = 0;
        int j = 0;
        for (j=0 ; j<(img->h) ; j++) {
            for(i=0 ; i<(img->w) ; i++) {
                int r = img->a[i][j].color.red;
                int g = img->a[i][j].color.green;
                int b = img->a[i][j].color.blue;
                int l = (r + g + b) / 3;
                img->a[i][j].brightness = l;
            }
        }
        img->type--;
        img->ext--;
    } else send_err("Format du fichier invalide");
}

/**
 * \fn void gray_level_advanced (t_img *img)
 * \brief Fonction permettant la transformation d'un image couleur en niveaux de gris
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

void gray_level_advanced (t_img *img) {
    if (img->ext == PPM && (img->type == P3 || img->type == P6)) {
        int i = 0;
        int j = 0;
        for (j=0 ; j<(img->h) ; j++) {
            for(i=0 ; i<(img->w) ; i++) {
                int r = img->a[i][j].color.red;
                int g = img->a[i][j].color.green;
                int b = img->a[i][j].color.blue;
                int l = 0.3 * r + 0.59 * g + 0.11 * b;
                img->a[i][j].brightness = l;
            }
        }
        img->type--;
        img->ext--;
    } else send_err("Format du fichier invalide");
}
