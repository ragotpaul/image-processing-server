/**
 * \file crop.c
 * \brief Fonctions de découpage d'une image
 * \version 1.0
 * \date 10/12/2015
 *
 * Contient toutes les fonctions permettant d'effectuer un crop sur une image.
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include "config.h"
#include "type.h"
#include "addon.h"
#include "ips.h"

#include "img.h"

#include "crop.h"

/**
 * \fn void crop (t_img *img, int x1, int y1, int x2, int y2)
 * \brief Fonction permettant de découper une image
 * @param[in]   x1  Entier représentant l'abscisse du point 1
 * @param[in]   y1  Entier représentant l'ordonnée du point 1
 * @param[in]   x2  Entier représentant l'abscisse du point 2
 * @param[in]   y2  Entier représentant l'ordonnée du point 2
 * @param[out]  img Pointeur d'un t_img correspondant à l'image
 */

void crop (t_img *img, int x1, int y1, int x2, int y2) {
    int i = 0;
    int j = 0;
    int x = 0;
    int y = 0;
    int min_x = min(x1, x2);
    int min_y = min(y1, y2);
    int max_x = max(x1, x2);
    int max_y = max(y1, y2);
    t_img *tmp = init_image();
    copy_image(img, tmp);
    erase_pixels(tmp);
    tmp->w = v_abs(x2 - x1) + 1;
    tmp->h = v_abs(y2 - y1) + 1;
    for (j = min_y ; j < max_y + 1 ; j++) {
        for (i = min_x ; i < max_x + 1 ; i++) {
            tmp->a[x][y] = img->a[i][j];
            x++;
        }
        x = 0;
        y++;
    }
    copy_image(tmp, img);
    free_image(tmp);
}
