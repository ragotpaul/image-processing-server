#ifndef EDGE_H
#define EDGE_H

void edge_sobel (t_img *img);
void edge_laplace (t_img *img);
void draw_edge (t_img *img);

#endif
