<?php
    session_start();
    $maxwidth = 2048;
    $maxheight = 2048;
    if (!(file_exists('img') AND is_dir('img'))) {
        mkdir('img');
    }
    if (isset($_FILES['imagefile']) AND $_FILES['imagefile']['error'] == 0) {
        $image_sizes = getimagesize($_FILES['imagefile']['tmp_name']);
        if ($image_sizes[0] <= $maxwidth AND $image_sizes[1] <= $maxheight) {
            if ($_FILES['imagefile']['size'] < 512000000) {
                $ext = pathinfo($_FILES['imagefile']['name'])['extension'];
                $format = array('ppm', 'pgm', 'pbm', 'jpg', 'jpeg', 'png', 'PPM', 'PGM', 'PBM', 'JPG', 'JPEG', 'PNG');
                if (in_array($ext, $format)) {
                    $_SESSION['ext'] = $ext;
                    $_SESSION['user_id'] = uniqid('user_');
                    $_SESSION['user_dir'] = 'img/'.$_SESSION['user_id'];
                    mkdir($_SESSION['user_dir']);
                    $_SESSION['filename'] = $_SESSION['user_dir'].'/in.'.$ext;
                    move_uploaded_file($_FILES['imagefile']['tmp_name'], $_SESSION['filename']);
                    switch ($ext) {
                        case 'ppm':
                        case 'PPM':
                        case 'pgm':
                        case 'PGM':
                        case 'pbm':
                        case 'PBM':
                            $_SESSION['fileview'] = $_SESSION['user_dir'].'/view.png';
                            $_SESSION['filepixmap'] = $_SESSION['user_dir'].'/pixmap.'.strtolower($ext);
                            exec('pnmtopng '.$_SESSION['filename'].' > '.$_SESSION['fileview']);
                            exec('rm '.$_SESSION['filename']);
                            exec('pngtopnm '.$_SESSION['fileview'].' > '.$_SESSION['filename']);
                            exec('cp '.$_SESSION['filename'].' '.$_SESSION['filepixmap']);
                            break;
                        case 'jpg':
                        case 'JPG':
                        case 'jpeg':
                        case 'JPEG':
                            $_SESSION['fileview'] = $_SESSION['user_dir'].'/view.png';
                            $_SESSION['filepixmap'] = $_SESSION['user_dir'].'/pixmap.ppm';
                            exec('jpegtopnm '.$_SESSION['filename'].' > '.$_SESSION['filepixmap']);
                            exec('pnmtopng '.$_SESSION['filepixmap'].' > '.$_SESSION['fileview']);
                            break;
                        case 'png':
                        case 'PNG':
                            $_SESSION['fileview'] = $_SESSION['user_dir'].'/view.png';
                            $_SESSION['filepixmap'] = $_SESSION['user_dir'].'/pixmap.ppm';
                            exec('cp '.$_SESSION['filename'].' '.$_SESSION['fileview']);
                            exec('pngtopnm -mix '.$_SESSION['fileview'].' > '.$_SESSION['filepixmap']);
                            break;
                        default:
                            $_SESSION['error'] = "Extension invalide";
                            break;
                    }
                } else {$_SESSION['error'] = "Extension invalide";}
            } else {$_SESSION['error'] = "Image trop volumineuse";}
        } else {$_SESSION['error'] = "Image trop grande";}
    } else {$_SESSION['error'] = "Echec de l'upload";}
    header('Location: index.php');
?>
