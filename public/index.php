<?php
    session_start();
    // print_r($_SESSION);
    foreach (glob('img/'."*") as $file) {
        if (filemtime($file) < time() - 86400) {
            exec('rm -fr '.$file);
            session_destroy();
            header('Refresh:0');
            }
    }
?>
<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <title>Image Processing Server</title>

        <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet" />
        <!-- <link href="css/bootstrap-theme.min.css" rel="stylesheet" /> -->

        <!-- Font Awesome -->
        <link href="css/font-awesome.min.css" rel="stylesheet" />

        <!-- Material -->
        <link href="css/bootstrap-material-design.min.css" rel="stylesheet" />

        <!-- Ripples -->
        <link href="css/ripples.min.css" rel="stylesheet" />

        <!-- Style Perso -->
        <link href="css/style.css" rel="stylesheet" />

        <!--[if lt IE 9]>
          <script src="js/html5shiv.min.js"></script>
          <script src="js/respond.min.js"></script>
        <![endif]-->
    </head>

    <body>
        <br />
        <div class="container">
            <h1 class="text-center">Image Processing Server</h1><hr/>
            <?php if (isset($_SESSION['error'])) { ?>
                <div class="alert alert-danger">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong>Erreur :</strong> <?php echo $_SESSION['error']; ?>
                </div>
            <?php unset($_SESSION['error']); } ?>
            <div class="col-lg-8">
                <?php if (isset($_SESSION['filename'])) { ?>
                    <?php if (isset($_SESSION['fileviewprocess'])) { ?>
                        <div class="well">
                            <form action="layer.php" method="POST" class="form">
                                <h4 class="text-center">Image modifié</h4><hr/>
                                <img src="<?php echo $_SESSION['fileviewprocess']; ?>" class="img-rounded img-responsive center-block" />
                                <br/>
                                <fieldset>
                                    <input type="hidden" name="layers" value="on" />
                                    <button type="submit" class="btn btn-fab btn-fab-mini pull-right">
                                        <i class="material-icons">layers</i>
                                        <div class="ripple-container"></div>
                                    </button>
                                </fieldset>
                            </form>
                        </div>
                    <?php } ?>
                    <div class="well">
                        <form action="destroy.php" method="POST" class="form">
                            <h4 class="text-center">Image original</h4><hr/>
                            <img src="<?php echo $_SESSION['fileview']; ?>" class="img-rounded img-responsive center-block" />
                            <br/>
                            <fieldset>
                                <button type="button" class="btn btn-fab btn-fab-mini pull-right" data-toggle="modal" data-target="#download">
                                    <i class="material-icons">file_download</i>
                                    <div class="ripple-container"></div>
                                </button>
                                <input type="hidden" name="reset_session" value="on" />
                                <button type="submit" class="btn btn-fab btn-fab-mini pull-right">
                                    <i class="material-icons">close</i>
                                    <div class="ripple-container"></div>
                                </button>
                            </fieldset>
                        </form>
                        <div id="download" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Télécharger votre fichier</h4>
                                    </div>
                                    <div class="modal-body text-center row">
                                        <div class="col-md-4">
                                            <form action="download.php" method="POST" class="form">
                                                <input type="hidden" name="format" value="pbm_ascii" />
                                                <button type="submit" class="btn btn-raised btn-lg btn-success">
                                                    <i class="material-icons">filter_1</i> PBM ASCII
                                            </form>
                                        </div>
                                        <div class="col-md-4">
                                            <form action="download.php" method="POST" class="form">
                                                <input type="hidden" name="format" value="pgm_ascii" />
                                                <button type="submit" class="btn btn-raised btn-lg btn-success">
                                                    <i class="material-icons">filter_2</i> PGM ASCII
                                            </form>
                                        </div>
                                        <div class="col-md-4">
                                            <form action="download.php" method="POST" class="form">
                                                <input type="hidden" name="format" value="ppm_ascii" />
                                                <button type="submit" class="btn btn-raised btn-lg btn-success">
                                                    <i class="material-icons">filter_3</i> PPM ASCII
                                            </form>
                                        <br />
                                        </div>
                                        <div class="col-md-4">
                                            <form action="download.php" method="POST" class="form">
                                                <input type="hidden" name="format" value="pbm_raw" />
                                                <button type="submit" class="btn btn-raised btn-lg btn-primary">
                                                    <i class="material-icons">filter_4</i> PBM RAW
                                            </form>
                                        </div>
                                        <div class="col-md-4">
                                            <form action="download.php" method="POST" class="form">
                                                <input type="hidden" name="format" value="pgm_raw" />
                                                <button type="submit" class="btn btn-raised btn-lg btn-primary">
                                                    <i class="material-icons">filter_5</i> PGM RAW
                                            </form>
                                        </div>
                                        <div class="col-md-4">
                                            <form action="download.php" method="POST" class="form">
                                                <input type="hidden" name="format" value="ppm_raw" />
                                                <button type="submit" class="btn btn-raised btn-lg btn-primary">
                                                    <i class="material-icons">filter_6</i> PPM RAW
                                            </form>
                                        <br />
                                        </div>
                                        <div class="col-md-offset-2 col-md-4">
                                            <form action="download.php" method="POST" class="form">
                                                <input type="hidden" name="format" value="png" />
                                                <button type="submit" class="btn btn-raised btn-lg btn-info">
                                                    <i class="material-icons">image</i> PNG
                                            </form>
                                        </div>
                                        <div class="col-md-4">
                                            <form action="download.php" method="POST" class="form">
                                                <input type="hidden" name="format" value="jpg" />
                                                <button type="submit" class="btn btn-raised btn-lg btn-info">
                                                    <i class="material-icons">photo_camera</i> JPG
                                            </form>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                <?php } else { ?>
                    <form action="upload.php" method="POST" enctype="multipart/form-data" class="form well">
                        <fieldset>
                            <legend>Charger une image</legend>
                            <div class="form-group is-empty is-fileinput">
                                <input type="text" readonly="" class="form-control" placeholder="Sélectionner une image (PNG, JPG, PBM, PGM ou PPM ; 2048x2048 max)" />
                                <input type="file" id="imagefile" name="imagefile" accept="image/x-portable-pixmap, image/x-portable-greymap, image/x-portablebitmap" required />
                            </div>
                            <span class="material-input"></span>
                            <button type="submit" class="btn btn-fab btn-fab-mini pull-right">
                                <i class="material-icons">file_upload</i>
                                <div class="ripple-container"></div>
                            </button>
                            <button type="reset" class="btn btn-fab btn-fab-mini pull-right">
                                <i class="material-icons">close</i>
                                <div class="ripple-container"></div>
                            </button>
                        </fieldset>
                    </form>
                <?php } ?>
            </div>
            <div class="col-lg-4">
                <form action="processing.php" method="POST" class="form well">
                    <fieldset>
                        <legend>Choix du traitement</legend>
                        <div class="form-group">
                            <h4>Simple</h4>
                            <div class="radio">
                                <label><input type="radio" name="process" value="binarisation" required /> Binarisation</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="process" value="negatif" required /> Négatif</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="process" value="gray_level" required /> Niveau de gris</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="process" value="increase_contrast" required /> Amélioration du contraste</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="process" value="symetry_h" required /> Symmétrie horizontale</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="process" value="symetry_v" required /> Symétrie verticale</label>
                            </div>
                            <h4>Filtrage par convolution</h4>
                            <div class="radio">
                                <label><input type="radio" name="process" value="smooth" required /> Smooth</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="process" value="gradient_x" required /> Gradient X</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="process" value="gradient_y" required /> Gradient Y</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="process" value="gradient" required /> Gradient</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="process" value="sobel_x" required /> Sobel X</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="process" value="sobel_y" required /> Sobel Y</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="process" value="sobel" required /> Sobel</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="process" value="laplace" required /> Laplace</label>
                            </div>
                            <h4>Détéction des contours</h4>
                            <div class="radio">
                                <label><input type="radio" name="process" value="edge_sobel" required /> Sobel</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="process" value="edge_laplace" required /> Laplace</label>
                            </div>
                            <div class="radio">
                                <label><input type="radio" name="process" value="edge_draw" required /> Dessin</label>
                            </div>
                        </div>
                        <span class="material-input"></span>
                        <button type="submit" class="btn btn-fab btn-fab-mini pull-right">
                            <i class="material-icons">photo_filter</i>
                            <div class="ripple-container"></div>
                        </button>
                        <button type="reset" class="btn btn-fab btn-fab-mini pull-right">
                            <i class="material-icons">close</i>
                            <div class="ripple-container"></div>
                        </button>
                    </fieldset>
                </form>
            </div>
        </div>

        <footer class="footer">
            <div class="container text-center">
                <a class="btn btn-raised btn-lg btn-info" href="app/build/html/index.html">
                    <i class="material-icons">web</i>
                    Documentation
                </a>
                <a class="btn btn-raised btn-lg btn-info" href="app/build/latex/refman.pdf">
                    <i class="material-icons">picture_as_pdf</i>
                    Documentation
                </a>
            </div>
        </footer>

        <!-- JavaScript -->

        <!-- Bootstrap -->
        <script src="js/bootstrap.min.js"></script>

        <!-- jQuery -->
        <script src="js/jquery-2.1.4.min.js"></script>

        <!-- Modal -->
        <script src="js/modal.js"></script>

        <!-- Material -->
        <script src="js/material.min.js"></script>

        <!-- Ripples -->
        <script src="js/ripples.min.js"></script>

        <!-- Script Perso -->
        <script src="js/script.js"></script>

    </body>
</html>
