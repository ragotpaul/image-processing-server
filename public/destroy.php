<?php
    session_start();
    if (isset($_POST['reset_session'])) {
        exec('rm -fr '.$_SESSION['user_dir']);
        session_destroy();
    }
    header('Location: index.php');
?>
