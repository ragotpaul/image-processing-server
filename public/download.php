<?php
    session_start();
    if (isset($_POST['format'])) {
        switch ($_POST['format']) {
            case 'pbm_ascii':
                exec('pngtopnm -mix '.$_SESSION['fileview'].' > '.$_SESSION['user_dir'].'/tmp.ppm');
                $in = $_SESSION['user_dir'].'/tmp.ppm';
                $out = $_SESSION['user_dir'].'/download';
                exec('./app/build/ips --convert P1 '.$in.' '.$out);
                unlink($in);
                $file = $out.'.pbm';
                break;
            case 'pgm_ascii':
                exec('pngtopnm -mix '.$_SESSION['fileview'].' > '.$_SESSION['user_dir'].'/tmp.ppm');
                $in = $_SESSION['user_dir'].'/tmp.ppm';
                $out = $_SESSION['user_dir'].'/download';
                exec('./app/build/ips --convert P2 '.$in.' '.$out);
                unlink($in);
                $file = $out.'.pgm';
                break;
            case 'ppm_ascii':
                exec('pngtopnm -mix '.$_SESSION['fileview'].' > '.$_SESSION['user_dir'].'/tmp.ppm');
                $in = $_SESSION['user_dir'].'/tmp.ppm';
                $out = $_SESSION['user_dir'].'/download';
                exec('./app/build/ips --convert P3 '.$in.' '.$out);
                unlink($in);
                $file = $out.'.ppm';
                break;
            case 'pbm_raw':
                exec('pngtopnm -mix '.$_SESSION['fileview'].' > '.$_SESSION['user_dir'].'/tmp.ppm');
                $in = $_SESSION['user_dir'].'/tmp.ppm';
                $out = $_SESSION['user_dir'].'/download';
                exec('./app/build/ips --convert P4 '.$in.' '.$out);
                unlink($in);
                $file = $out.'.pbm';
                break;
            case 'pgm_raw':
                exec('pngtopnm -mix '.$_SESSION['fileview'].' > '.$_SESSION['user_dir'].'/tmp.ppm');
                $in = $_SESSION['user_dir'].'/tmp.ppm';
                $out = $_SESSION['user_dir'].'/download';
                exec('./app/build/ips --convert P5 '.$in.' '.$out);
                unlink($in);
                $file = $out.'.pgm';
                break;
            case 'ppm_raw':
                exec('pngtopnm -mix '.$_SESSION['fileview'].' > '.$_SESSION['user_dir'].'/tmp.ppm');
                $in = $_SESSION['user_dir'].'/tmp.ppm';
                $out = $_SESSION['user_dir'].'/download';
                exec('./app/build/ips --convert P6 '.$in.' '.$out);
                unlink($in);
                $file = $out.'.ppm';
                break;
            case 'png':
                $file = $_SESSION['fileview'];
                break;
            case 'jpg':
                exec('pngtopnm -mix '.$_SESSION['fileview'].' > '.$_SESSION['user_dir'].'/tmp.ppm');
                $in = $_SESSION['user_dir'].'/tmp.ppm';
                $out = $_SESSION['user_dir'].'/download';
                exec('pnmtojpeg '.$in.' > '.$out.'.jpg');
                unlink($in);
                $file = $out.'.jpg';
                break;
            default:
                $_SESSION['error'] = 'Format invalide';
                break;
        }
        header('Location: '.$file);
    }
?>
